{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $infos|@count > 0}
    <!-- MODULE Block customreinsurance -->
    <div id="customreinsurance_block" class="ass_global clearfix">
        {*	<ul class="width{$nbblocks}">	
        {foreach from=$infos item=info}
        <li><img src="{$link->getMediaLink("`$module_dir`img/`$info.file_name|escape:'htmlall':'UTF-8'`")}" alt="{$info.text|escape:html:'UTF-8'}" /> <span>{$info.text|escape:html:'UTF-8'}</span></li>                        
        {/foreach}
        </ul>*}
        <div id="ass_global" style="display: block;">
            {foreach from=$infos item=info key=k}  
                {$rank = $k + 1}
                <aside id="ass_bloc{$rank}">
                    <a class="iframe" href="#" title="{$info.text|escape:html:'UTF-8'}"><div>
                            <span class="bleu">{$info.text|escape:html:'UTF-8'}</span>
                            
                        </div></a>
                </aside>
            {/foreach}            
        </div>

    </div>
    <!-- /MODULE Block customreinsurance -->
{/if}