//added from html file
var txtTimeShip = 'D&eacute;lai allong&eacute; de 24h pour toute d&eacute;coupe sur mesure Dimexact';
var txtFreeShip = 'Vous b&eacute;n&eacute;ficiez de la livraison OFFERTE en relais Colis Pickup ! Le montant de votre panier est sup&eacute;rieur &agrave; %s &euro;.';
var txtNoFreeShip = 'Il manque %s &euro; pour avoir une livraison en relais Colis Pickup OFFERTE !';

var txtValidateRequired = 'Champ requis !';
var txtValidateEmailSyntax = 'V&eacute;rifier la syntaxe de l&#039;adresse email';
var txtValidateSyntax = 'V&eacute;rifier la syntaxe du contenu du champ';
var txtValidateNumberPhone = 'Veuillez renseigner au moins un num&eacute;ro de t&eacute;l&eacute;phone';
//end added from html file
function updateFormDatas()
{
    var nb = $('#quantity_wanted').val();
    var id = $('#idCombination').val();

    $('.paypal_payment_form input[name=quantity]').val(nb);
    $('.paypal_payment_form input[name=id_p_attr]').val(id);
}

$(document).ready(function () {

    if ($('#in_context_checkout_enabled').val() != 1)
    {
        $('#payment_paypal_express_checkout').click(function () {
            $('#paypal_payment_form_cart').submit();
            return false;
        });
    }


    var jquery_version = $.fn.jquery.split('.');
    if (jquery_version[0] >= 1 && jquery_version[1] >= 7)
    {
        $('body').on('submit', ".paypal_payment_form", function () {
            updateFormDatas();
        });
    } else {
        $('.paypal_payment_form').live('submit', function () {
            updateFormDatas();
        });
    }

    function displayExpressCheckoutShortcut() {
        var id_product = $('input[name="id_product"]').val();
        var id_product_attribute =
                $('input[name="id_product_attribute"]').val();
        $.ajax({
            type: "GET",
            url: baseDir + '/modules/paypal/express_checkout/ajax.php',
            data: {get_qty: "1", id_product: id_product, id_product_attribute:
                        id_product_attribute},
            cache: false,
            success: function (result) {
                if (result == '1') {
                    $('#container_express_checkout').slideDown();
                } else {
                    $('#container_express_checkout').slideUp();
                }
                return true;
            }
        });
    }

    $('select[name^="group_"]').change(function () {
        setTimeout(function () {
            displayExpressCheckoutShortcut()
        }, 500);
    });

    $('.color_pick').click(function () {
        setTimeout(function () {
            displayExpressCheckoutShortcut()
        }, 500);
    });

    if ($('body#product').length > 0)
        setTimeout(function () {
            displayExpressCheckoutShortcut()
        }, 500);




    var modulePath = 'modules/paypal';
    var subFolder = '/integral_evolution';

    var baseDirPP = baseDir.replace('http:', 'https:');

    var fullPath = baseDirPP + modulePath + subFolder;
    var confirmTimer = false;

    if ($('form[target="hss_iframe"]').length == 0) {
        if ($('select[name^="group_"]').length > 0)
            displayExpressCheckoutShortcut();
        return false;
    } else {
        checkOrder();
    }

    function checkOrder() {
        if (confirmTimer == false)
            confirmTimer = setInterval(getOrdersCount, 1000);
    }

    function getOrdersCount() {


        $.get(
                fullPath + '/confirm.php',
                {id_cart: '151390'},
                function (data) {
                    if ((typeof (data) != 'undefined') && (data > 0)) {
                        clearInterval(confirmTimer);
                        window.location.replace(fullPath + '/submit.php?id_cart=151390');
                        $('p.payment_module, p.cart_navigation').hide();
                    }
                }
        );
    }
});
// <![CDATA[
var currencySign = '€';
var currencyRate = 1;
var currencyFormat = 2;
var currencyBlank = 1;
var txtProduct = "produit";
var txtProducts = "produits";
var deliveryAddress = 0;
//]]>
var dataCarriers = {};
// <![CDATA[
var taxEnabled = "1";
var displayPrice = "0";
var currencySign = '€';
var currencyRate = 1;
var currencyFormat = 2;
var currencyBlank = 1;
var id_state = 0;
var SE_RedirectTS = "Rechargement de la page et mise à jour du panier...";
var SE_RefreshStateTS = "Vérification des états disponibles...";
var SE_RetrievingInfoTS = "Récupération des informations...";
var SE_RefreshMethod = 0;
var txtFree = "Offert !";

var txtTimeShip = 'Délai allongé de 24h pour toute découpe sur mesure Dimexact';
var txtFreeShip = 'Vous bénéficiez de la livraison OFFERTE en relais Colis Pickup ! Le montant de votre panier est supérieur à %s €.';
var txtNoFreeShip = 'Il manque %s € pour avoir une livraison en relais Colis Pickup OFFERTE !';

var id_carrier = 143;
var countriesNeedZipCode = new Array();
//]]>
// <![CDATA[
//idSelectedCountry = false;

countriesNeedZipCode[1] = 1;

countriesNeedZipCode[3] = 1;

countriesNeedZipCode[69] = 1;

countriesNeedZipCode[6] = 1;

countriesNeedZipCode[8] = 1;

countriesNeedZipCode[98] = 1;

countriesNeedZipCode[241] = 1;

countriesNeedZipCode[10] = 1;

countriesNeedZipCode[12] = 1;

countriesNeedZipCode[141] = 1;

countriesNeedZipCode[144] = 1;

countriesNeedZipCode[148] = 1;

countriesNeedZipCode[242] = 1;

countriesNeedZipCode[15] = 1;

countriesNeedZipCode[176] = 1;

countriesNeedZipCode[179] = 1;

countriesNeedZipCode[182] = 1;

countriesNeedZipCode[19] = 1;
//]]>
//<![CDATA[
var txtFree = "Offert !";
//]]>
//<![CDATA[
var dpd_idCarrier = [161, 160, 139];
var dpd_idContent = ['dpdfrance_relais_container',
    'tr_carrier_dpdfrance_relais', 'dpdfrance_predict_container',
    'tr_carrier_predict'];

//---
$('document').ready(function () {
    $(document).on('click', "#carrier_area .delivery_option", function (evt)
    {

        fpv_checkSelected($(this));
        updateCarrierSelectionAndGift();
    });
    fpv_checkSelected();
});
//---
function fpv_checkSelected($o) {
    if (!$o)
        $o = $(".delivery_option_radio:checked").parents(".delivery_option");
    else
        $("#carrier_area .delivery_option").removeClass('checked');
    $o.addClass('checked');
}
//---
function fpv_isCarrierDPD($o) {
    for (var i = 0; i < dpd_idCarrier.length; i++) {
        if (parseInt($o.find(".delivery_option_radio").val()) ==
                dpd_idCarrier[i])
            return true;
    }
    return false;
}
//---
function fpv_closePopUpDPD(evt) {
    if ((typeof evt == 'object' && evt.which && $.inArray(evt.target.id,
            dpd_idContent) > -1) || evt === true)
        $("#center_column").addClass('no-pop-up');
    else
        $("#center_column").removeClass('no-pop-up');
}


var time_start;
$(window).load(
        function () {
            time_start = new Date();
        }
);
//end added from html file
function updateCarrierList(json)
{
    var html = json.carrier_block;

    $('#carrier_area').replaceWith(html);
    bindInputs();
    /* update hooks for carrier module */
    $('#HOOK_BEFORECARRIER').html(json.HOOK_BEFORECARRIER);

    cc_updateDisplay();

    if (typeof (updateCarriersList) !== 'undefined' && !isLogged)
        updateCarriersList(json);
}

function updatePaymentMethods(json)
{
    $('#HOOK_TOP_PAYMENT').html(json.HOOK_TOP_PAYMENT);
    $('#opc_payment_methods-content div#HOOK_PAYMENT').html(json.HOOK_PAYMENT);

    // Mise à jour de la prévisualisation des moyens de paiement
    if (typeof json.htmlPaymentPreview !== 'undefined')
        $("#HOOK_PAYMENT_PREVIEW").html(json.htmlPaymentPreview);

    opcPaymentRefresh();
}

function updateAddressSelection()
{
    var idAddress_delivery = ($('input#opc_id_address_delivery').length == 1 ? $('input#opc_id_address_delivery').val() : $('#id_address_delivery').val());
    var idAddress_invoice = ($('input#opc_id_address_invoice').length == 1 ? $('input#opc_id_address_invoice').val() : ($('input[type=checkbox]#addressesAreEquals:checked').length == 1 ? idAddress_delivery : ($('#id_address_invoice').length == 1 ? $('select#id_address_invoice').val() : idAddress_delivery)));

    $('#opc_account-overlay').fadeIn('slow');
    $('#opc_delivery_methods-overlay').fadeIn('slow');
    $('#opc_payment_methods-overlay').fadeIn('slow');

    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "json",
        data: 'ajax=true&method=updateAddressesSelected&id_address_delivery=' + idAddress_delivery + '&id_address_invoice=' + idAddress_invoice + '&token=' + static_token,
        success: function (jsonData)
        {
            if (jsonData.hasError)
            {
                var errors = '';
                for (error in jsonData.errors)
                    //IE6 bug fix
                    if (error != 'indexOf')
                        errors += jsonData.errors[error] + "\n";
                alert(errors);
            } else
            {
                // Update all product keys with the new address id
                $('#cart_summary .address_' + deliveryAddress).each(function () {
                    $(this)
                            .removeClass('address_' + deliveryAddress)
                            .addClass('address_' + idAddress_delivery);
                    $(this).attr('id', $(this).attr('id').replace(/_\d+$/, '_' + idAddress_delivery));
                    if ($(this).find('.cart_unit span').length > 0 && $(this).find('.cart_unit span').attr('id').length > 0)
                        $(this).find('.cart_unit span').attr('id', $(this).find('.cart_unit span').attr('id').replace(/_\d+$/, '_' + idAddress_delivery));

                    if ($(this).find('.cart_total span').length > 0 && $(this).find('.cart_total span').attr('id').length > 0)
                        $(this).find('.cart_total span').attr('id', $(this).find('.cart_total span').attr('id').replace(/_\d+$/, '_' + idAddress_delivery));

                    if ($(this).find('.cart_quantity_input').length > 0 && $(this).find('.cart_quantity_input').attr('name').length > 0)
                    {
                        name = $(this).find('.cart_quantity_input').attr('name') + '_hidden';
                        $(this).find('.cart_quantity_input').attr('name', $(this).find('.cart_quantity_input').attr('name').replace(/_\d+$/, '_' + idAddress_delivery));
                        if ($(this).find('[name=' + name + ']').length > 0)
                            $(this).find('[name=' + name + ']').attr('name', name.replace(/_\d+_hidden$/, '_' + idAddress_delivery + '_hidden'));
                    }

                    if ($(this).find('.cart_quantity_delete').length > 0 && $(this).find('.cart_quantity_delete').attr('id').length > 0) {
                        $(this).find('.cart_quantity_delete')
                                .attr('id', $(this).find('.cart_quantity_delete').attr('id').replace(/_\d+$/, '_' + idAddress_delivery))
                                .attr('href', $(this).find('.cart_quantity_delete').attr('href').replace(/id_address_delivery=\d+&/, 'id_address_delivery=' + idAddress_delivery + '&'))
                    }

                    if ($(this).find('.cart_quantity_down').length > 0 && $(this).find('.cart_quantity_down').attr('id').length > 0) {
                        $(this).find('.cart_quantity_down')
                                .attr('id', $(this).find('.cart_quantity_down').attr('id').replace(/_\d+$/, '_' + idAddress_delivery))
                                .attr('href', $(this).find('.cart_quantity_down').attr('href').replace(/id_address_delivery=\d+&/, 'id_address_delivery=' + idAddress_delivery + '&'))
                    }

                    if ($(this).find('.cart_quantity_up').length > 0 && $(this).find('.cart_quantity_up').attr('id').length > 0) {
                        $(this).find('.cart_quantity_up')
                                .attr('id', $(this).find('.cart_quantity_up').attr('id').replace(/_\d+$/, '_' + idAddress_delivery))
                                .attr('href', $(this).find('.cart_quantity_up').attr('href').replace(/id_address_delivery=\d+&/, 'id_address_delivery=' + idAddress_delivery + '&'))
                    }

                });

                // Update global var deliveryAddress
                deliveryAddress = idAddress_delivery;

                updateCarrierList(jsonData.carrier_data);
                updatePaymentMethods(jsonData);
                updateCartSummary(jsonData.summary);
                updateHookShoppingCart(jsonData.HOOK_SHOPPING_CART);
                updateHookShoppingCartExtra(jsonData.HOOK_SHOPPING_CART_EXTRA);
                if ($('#gift-price').length == 1)
                    $('#gift-price').html(jsonData.gift_price);
                $('#opc_account-overlay').fadeOut('slow');
                $('#opc_delivery_methods-overlay').fadeOut('slow');
                $('#opc_payment_methods-overlay').fadeOut('slow');

                aliasUpdateAddressesSelected = true;
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus != 'abort')
                alert("TECHNICAL ERROR: unable to save adresses \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
            $('#opc_account-overlay').fadeOut('slow');
            $('#opc_delivery_methods-overlay').fadeOut('slow');
            $('#opc_payment_methods-overlay').fadeOut('slow');
        }
    });
}

function getCarrierListAndUpdate()
{
    $('#opc_delivery_methods-overlay').fadeIn('slow');
    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "json",
        data: 'ajax=true&method=getCarrierList&token=' + static_token,
        success: function (jsonData)
        {
            if (jsonData.hasError)
            {
                var errors = '';
                for (error in jsonData.errors)
                    //IE6 bug fix
                    if (error != 'indexOf')
                        errors += jsonData.errors[error] + "\n";
                alert(errors);
            } else
                updateCarrierList(jsonData);
            $('#opc_delivery_methods-overlay').fadeOut('slow');
        }
    });
}

function updateCarrierSelectionAndGift()
{
    var recyclablePackage = 0;
    var gift = 0;
    var giftMessage = '';

    var delivery_option_radio = $('.delivery_option_radio');
    var delivery_option_params = '&';
    $.each(delivery_option_radio, function (i) {
        if ($(this).prop('checked'))
            delivery_option_params += $(delivery_option_radio[i]).attr('name') + '=' + $(delivery_option_radio[i]).val() + '&';
    });
    if (delivery_option_params == '&')
        delivery_option_params = '&delivery_option=&'

    if ($('input#recyclable:checked').length)
        recyclablePackage = 1;
    if ($('input#gift:checked').length)
    {
        gift = 1;
        giftMessage = encodeURIComponent($('textarea#gift_message').val());
    }

    $('#opc_payment_methods-overlay').fadeIn('slow');
    $('#opc_delivery_methods-overlay').fadeIn('slow');
    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "json",
        data: 'ajax=true&method=updateCarrierAndGetPayments' + delivery_option_params + 'recyclable=' + recyclablePackage + '&gift=' + gift + '&gift_message=' + giftMessage + '&token=' + static_token,
        success: function (jsonData)
        {
            if (jsonData.hasError)
            {
                var errors = '';
                for (error in jsonData.errors)
                    //IE6 bug fix
                    if (error != 'indexOf')
                        errors += jsonData.errors[error] + "\n";
                alert(errors);
            } else
            {
                updateCartSummary(jsonData.summary);
                updatePaymentMethods(jsonData);
                updateHookShoppingCart(jsonData.summary.HOOK_SHOPPING_CART);
                updateHookShoppingCartExtra(jsonData.summary.HOOK_SHOPPING_CART_EXTRA);
                updateCarrierList(jsonData.carrier_data);
                $('#opc_payment_methods-overlay').fadeOut('slow');
                $('#opc_delivery_methods-overlay').fadeOut('slow');
                refreshDeliveryOptions();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            //if (textStatus != 'abort')
            //	alert("TECHNICAL ERROR: unable to save carrier \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
            $('#opc_payment_methods-overlay').fadeOut('slow');
            $('#opc_delivery_methods-overlay').fadeOut('slow');
        }
    });
}

function confirmFreeOrder()
{
    if ($('#opc_new_account-overlay').length != 0)
        $('#opc_new_account-overlay').fadeIn('slow');
    else
        $('#opc_account-overlay').fadeIn('slow');
    $('#opc_delivery_methods-overlay').fadeIn('slow');
    $('#opc_payment_methods-overlay').fadeIn('slow');
    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "html",
        data: 'ajax=true&method=makeFreeOrder&token=' + static_token,
        success: function (html)
        {
            var array_split = html.split(':');
            if (array_split[0] === 'freeorder')
            {
                if (isGuest)
                    document.location.href = guestTrackingUrl + '?id_order=' + encodeURIComponent(array_split[1]) + '&email=' + encodeURIComponent(array_split[2]);
                else
                    document.location.href = historyUrl;
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus != 'abort')
                alert("TECHNICAL ERROR: unable to confirm the order \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
        }
    });
}

function saveAddress(type, id, editType)
{
    if (type != 'delivery' && type != 'invoice' && type != 'edit')
        return false;

    if (typeof id != 'number')
        id = 0;

    var tmp_same_address = $('input[type=checkbox]#addressesAreEquals:checked').length == 1 ? true : false;
    var tmp_id_address_invoice = tmp_same_address ? $('#id_address_delivery').val() : $('#id_address_invoice').val();

    var params = 'firstname=' + encodeURIComponent($('#firstname' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'lastname=' + encodeURIComponent($('#lastname' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'company=' + encodeURIComponent($('#company' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'vat_number=' + encodeURIComponent($('#vat_number' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'dni=' + encodeURIComponent($('#dni' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'address1=' + encodeURIComponent($('#address1' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'address2=' + encodeURIComponent($('#address2' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'postcode=' + encodeURIComponent($('#postcode' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'city=' + encodeURIComponent($('#city' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'id_country=' + encodeURIComponent($('#id_country').val()) + '&';
    if ($('#id_state' + (type == 'invoice' ? '_invoice' : '')).val())
    {
        params += 'id_state=' + encodeURIComponent($('#id_state' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    }
    params += 'other=' + encodeURIComponent($('#other' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'phone=' + encodeURIComponent($('#phone' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';
    params += 'phone_mobile=' + encodeURIComponent($('#phone_mobile' + (type == 'invoice' ? '_invoice' : '')).val()) + '&';

    var index = null;
    var alias = $('#alias' + (type == 'invoice' ? '_invoice' : '')).val();
    if (type == 'edit') {
        if (alias != aliasEdit.value) {
            index = aliasIndex(aliasEdit.value);
            alias = aliasGet(alias, index);
        }
    }
    params += 'alias=' + encodeURIComponent(alias) + '&';

    // Clean the last &
    params = params.substr(0, params.length - 1);

    var param_type = '';
    if (type == 'edit') {
        param_type = '&id_address=' + id + '&select_address=1';
    } else
        param_type = '&type=' + type;

    var result = false;

    $.ajax({
        type: 'POST',
        url: addressUrl,
        async: false,
        cache: false,
        dataType: "json",
        data: 'ajax=true&submitAddress=true' + param_type + '&' + params + '&token=' + static_token,
        success: function (jsonData)
        {
            if (jsonData.hasError)
            {
                var tmp = '';
                var i = 0;
                for (error in jsonData.errors)
                    //IE6 bug fix
                    if (error != 'indexOf')
                    {
                        i = i + 1;
                        tmp += '<li>' + jsonData.errors[error] + '</li>';
                    }
                tmp += '</ol>';
                var errors = '<b>' + txtThereis + ' ' + i + ' ' + txtErrors + ':</b><ol>' + tmp;
                $('#opc_account_errors').html(errors).slideDown('slow');
                $.scrollTo('#opc_account_errors', 800);
                $('#opc_new_account-overlay').fadeOut('slow');
                $('#opc_delivery_methods-overlay').fadeOut('slow');
                $('#opc_payment_methods-overlay').fadeOut('slow');
                result = false;
            } else
            {
                if (type == 'edit') {
                    if (index == null)
                        aliasAddressList.push(alias);
                    else
                        aliasAddressList[index] = alias;
                }

                if (editType != null) {
                    if (editType == 'delivery') {
                        $("<input>")
                                .attr('type', 'hidden')
                                .attr('id', 'opc_id_address_delivery')
                                .val(jsonData.id_address_invoice)
                                .appendTo("#opc_new_account");

                        var id_inv = tmp_same_address ? jsonData.id_address_invoice : tmp_id_address_invoice;

                        $("<input>")
                                .attr('type', 'hidden')
                                .attr('id', 'opc_id_address_invoice')
                                .val(id_inv)
                                .appendTo("#opc_new_account");

                        updateAddressSelection();

                        $('input#opc_id_address_delivery').remove();
                        $('input#opc_id_address_invoice').remove();
                    } else
                        aliasUpdateAddressesSelected = true;
                } else {
                    // update addresses id
                    $('input#opc_id_address_delivery').val(jsonData.id_address_delivery);
                    $('input#opc_id_address_invoice').val(jsonData.id_address_invoice);
                }
                result = true;
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            if (textStatus != 'abort')
                alert("TECHNICAL ERROR: unable to save adresses \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
            $('#opc_new_account-overlay').fadeOut('slow');
            $('#opc_delivery_methods-overlay').fadeOut('slow');
            $('#opc_payment_methods-overlay').fadeOut('slow');
        }
    });

    return result;
}

function updateNewAccountToAddressBlock(loadStep)
{

    $("#opc_new_account").addClass('on_load');


    $('#opc_new_account-overlay').fadeIn('slow');
    $('#opc_delivery_methods-overlay').fadeIn('slow');
    $('#opc_payment_methods-overlay').fadeIn('slow');
    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "json",
        data: 'ajax=true&method=getAddressBlockAndCarriersAndPayments&token=' + static_token,
        success: function (json)
        {
            isLogged = 1;

            if (json.no_address == 1)
                document.location.href = addressUrl;

            if (isLogged) {
                $("#BLOCK_SHIPPING_CELL").addClass('no-country-simulate')
                $("#center_column").addClass('is_logged');
            }
            $('#opc_new_account').fadeOut('fast', function () {
                $('#opc_new_account').html(json.order_opc_adress);
                // update block user info
                if (json.block_user_info != '' && $('#header_user').length == 1)
                {
                    $('#header_user').fadeOut('slow', function () {
                        $(this).attr('id', 'header_user_old').after(json.block_user_info).fadeIn('slow');
                        $('#header_user_old').remove();
                    });
                }
                $('#opc_new_account').fadeIn('fast', function () {

                    //After login, the products are automatically associated to an address
                    $.each(json.summary.products, function () {
                        updateAddressId(this.id_product, this.id_product_attribute, '0', this.id_address_delivery);
                    });

                    updateCartSummary(json.summary);
                    updateAddressesDisplay(true);
                    updateCarrierList(json.carrier_data);
                    updatePaymentMethods(json);
                    if ($('#gift-price').length == 1)
                        $('#gift-price').html(json.gift_price);
                    $('#opc_delivery_methods-overlay').fadeOut('slow');
                    $('#opc_payment_methods-overlay').fadeOut('slow');

                    if (loadStep)
                        opcStep(4);

                });


            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
//			if (textStatus != 'abort')
//				alert("TECHNICAL ERROR: unable to send login informations \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
            $('#opc_delivery_methods-overlay').fadeOut('slow');
            $('#opc_payment_methods-overlay').fadeOut('slow');

            $("#opc_new_account").removeClass('on_load');
        }
    });
}

$(function () {

    if (isLogged) {
        $("#BLOCK_SHIPPING_CELL").addClass('no-country-simulate')
        $("#center_column").addClass('is_logged');
    }

    // GUEST CHECKOUT / NEW ACCOUNT MANAGEMENT
    if ((!isLogged) || (isGuest))
    {
        if (guestCheckoutEnabled && !isLogged)
        {
            $('#opc_account_choice').show();
            $('#opc_account_form').hide();
            $('#opc_invoice_address').hide();

            $('#opc_createAccount').click(function () {
                $('.is_customer_param').show();
                $('#opc_account_form').slideDown('slow');
                $('#is_new_customer').val('1');
                $('#opc_account_choice').hide();
                $('#opc_invoice_address').hide();
                updateState();
                updateNeedIDNumber();
                updateZipCode();
            });
            $('#opc_guestCheckout').click(function () {
                $('.is_customer_param').hide();
                $('#opc_account_form').slideDown('slow');
                $('#is_new_customer').val('0');
                $('#opc_account_choice').hide();
                $('#opc_invoice_address').hide();
                $('#new_account_title').html(txtInstantCheckout);
                updateState();
                updateNeedIDNumber();
                updateZipCode();
            });
        } else if (isGuest)
        {
            $('.is_customer_param').hide();
            $('#opc_account_form').show('slow');
            $('#is_new_customer').val('0');
            $('#opc_account_choice').hide();
            $('#opc_invoice_address').hide();
            $('#new_account_title').html(txtInstantCheckout);
            updateState();
            updateNeedIDNumber();
            updateZipCode();
        } else
        {
            $('#opc_account_choice').hide();
            $('#is_new_customer').val('1');
            $('.is_customer_param').show();
            $('#opc_account_form').show();
            $('#opc_invoice_address').hide();
            updateState();
            updateNeedIDNumber();
            updateZipCode();
        }

        // LOGIN FORM
        $('#openLoginFormBlock').click(function () {
            $('#openNewAccountBlock').show();
            $(this).hide();
            $('#login_form_content').slideDown('slow');
            $('#new_account_form_content').slideUp('slow');
            return false;
        });
        // LOGIN FORM SENDING
        $('#SubmitLogin').click(function () {
            $.ajax({
                type: 'POST',
                url: authenticationUrl,
                async: false,
                cache: false,
                dataType: "json",
                data: 'SubmitLogin=true&ajax=true&email=' + encodeURIComponent($('#login_email').val()) + '&passwd=' + encodeURIComponent($('#login_passwd').val()) + '&token=' + static_token,
                success: function (jsonData)
                {
                    if (jsonData.hasError)
                    {
                        var errors = '<b>' + txtThereis + ' ' + jsonData.errors.length + ' ' + txtErrors + ':</b><ol>';
                        for (error in jsonData.errors)
                            //IE6 bug fix
                            if (error != 'indexOf')
                                errors += '<li>' + jsonData.errors[error] + '</li>';
                        errors += '</ol>';
                        $('#opc_login_errors').html(errors).slideDown('slow');
                    } else
                    {
                        isLogged = true;

                        // update token
                        static_token = jsonData.token;
                        updateNewAccountToAddressBlock();

                        opcStep(3);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (textStatus != 'abort')
                        alert("TECHNICAL ERROR: unable to send login informations \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
                }
            });
            return false;
        });

        // INVOICE ADDRESS
        $('#invoice_address').click(function () {
            if ($('#invoice_address:checked').length > 0)
            {
                $('#opc_invoice_address').slideDown('slow');
                if ($('#company_invoice').val() == '')
                    $('#vat_number_block_invoice').hide();
                updateState('invoice');
                updateNeedIDNumber('invoice');
                updateZipCode('invoice');
            } else
                $('#opc_invoice_address').slideUp('slow');
        });

        // VALIDATION / CREATION AJAX
        $('#submitAccount').click(function (evt) {

            if (!opcValidateAddress()) {
                evt.stopPropagation();
                evt.preventDefault();
                return false;
            }
            $('#opc_new_account-overlay').fadeIn('slow');
            $('#opc_delivery_methods-overlay').fadeIn('slow');
            $('#opc_payment_methods-overlay').fadeIn('slow');

            // RESET ERROR(S) MESSAGE(S)
            $('#opc_account_errors').html('').slideUp('slow');
            var isNew = ($('input#opc_id_customer').val() == 0);
            if (isNew)
            {
                var callingFile = authenticationUrl;
                var params = 'submitAccount=true&';
            } else
            {
                var callingFile = orderOpcUrl;
                var params = 'method=editCustomer&';
            }

            $('#opc_account_form input:visible, #opc_account_form input[type=hidden]').each(function ()
            {
                var name = $(this).attr('name');

                if (name != "alias" || name != "alias_invoice")
                {
                    if ($(this).is('input[type=checkbox]'))
                    {
                        if ($(this).is(':checked'))
                            params += encodeURIComponent(name) + '=1&';
                    } else if ($(this).is('input[type=radio]'))
                    {
                        if ($(this).is(':checked'))
                            params += encodeURIComponent(name) + '=' + encodeURIComponent($(this).val()) + '&';
                    } else
                        params += encodeURIComponent(name) + '=' + encodeURIComponent($(this).val()) + '&';
                }
            });

            $('#opc_account_form select:visible').each(function () {
                params += encodeURIComponent($(this).attr('name')) + '=' + encodeURIComponent($(this).val()) + '&';
            });

            params += 'other=' + encodeURIComponent($('#other').val()) + '&';

            // Récupération & Gestion du champ "Alias" de livraison
            console.log("alias");
//			var alias = aliasGetInputVal();
//			params += 'alias='+ encodeURIComponent(alias) +'&';

            $("#invoice_address").is(':checked')
            {
                params += 'other_invoice=' + encodeURIComponent($('#other_invoice').val()) + '&';

                // Récupération & Gestion du champ "Alias" de facturation
//				var alias_invoice = aliasGetInputVal(true);
//				params += 'alias_invoice='+ encodeURIComponent(alias_invoice) +'&';
            }

            params += 'customer_lastname=' + encodeURIComponent($('#customer_lastname').val()) + '&';
            params += 'customer_firstname=' + encodeURIComponent($('#customer_firstname').val()) + '&';

            params += 'email=' + encodeURIComponent($("#email").val()) + '&';
            params += 'passwd=' + encodeURIComponent($("#passwd").val()) + '&';

            params += 'is_new_customer=' + encodeURIComponent($('#is_new_customer').val()) + '&';

            // Clean the last &
            params = params.substr(0, params.length - 1);

            $.ajax({
                type: 'POST',
                url: callingFile,
                async: false,
                cache: false,
                dataType: "json",
                data: 'ajax=true&' + params + '&token=' + static_token,
                success: function (jsonData)
                {
                    if (jsonData.hasError)
                    {
                        // ré-affectation des valeurs des champs "Alias"
                        aliasReassignInput();

                        var tmp = '';
                        var i = 0;
                        for (error in jsonData.errors)
                            //IE6 bug fix
                            if (error != 'indexOf')
                            {
                                i = i + 1;
                                tmp += '<li>' + jsonData.errors[error] + '</li>';
                            }
                        tmp += '</ol>';
                        var errors = '<b>' + txtThereis + ' ' + i + ' ' + txtErrors + ':</b><ol>' + tmp;
                        $('#opc_account_errors').html(errors).slideDown('slow');
                        $.scrollTo('#opc_account_errors', 800);

                        $("#opc_new_account").removeClass('on_load');
                    }

                    // Nettoyage de la mémorisation des valeurs des champs "Alias"
                    aliasCleanInitialAssignment();

                    isGuest = ($('#is_new_customer').val() == 1 ? 0 : 1);

                    if (jsonData.id_customer != undefined && jsonData.id_customer != 0 && jsonData.isSaved)
                    {
                        // update token
                        static_token = jsonData.token;

                        // update addresses id
                        $('input#opc_id_address_delivery').val(jsonData.id_address_delivery);
                        $('input#opc_id_address_invoice').val(jsonData.id_address_invoice);

                        // It's not a new customer
                        if ($('input#opc_id_customer').val() != '0')
                        {
                            if (!saveAddress('delivery'))
                                return false;
                        }

                        // update id_customer
                        $('input#opc_id_customer').val(jsonData.id_customer);

                        if ($('#invoice_address:checked').length != 0)
                        {
                            if (!saveAddress('invoice'))
                                return false;
                        }

                        // update id_customer
                        $('input#opc_id_customer').val(jsonData.id_customer);

                        // force to refresh carrier list
                        if (isGuest)
                        {
                            isLogged = 1;
                            $('#opc_account_saved').fadeIn('slow');
                            $('#submitAccount').hide();
                            updateAddressSelection();
                        } else {
                            updateNewAccountToAddressBlock(true);
                            $("#opc_new_account").removeClass('on_load');
                        }

                    } else
                        $("#opc_new_account").removeClass('on_load');

                    $('#opc_new_account-overlay').fadeOut('slow');
                    $('#opc_delivery_methods-overlay').fadeOut('slow');
                    $('#opc_payment_methods-overlay').fadeOut('slow');
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    if (textStatus != 'abort')
                        alert("TECHNICAL ERROR: unable to save account \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
                    $('#opc_new_account-overlay').fadeOut('slow');
                    $('#opc_delivery_methods-overlay').fadeOut('slow');
                    $('#opc_payment_methods-overlay').fadeOut('slow');

                    $("#opc_new_account").removeClass('on_load');
                }
            });
            return false;
        });
    }

    bindInputs();

    $('#opc_account_form input,select,textarea').change(function () {
        if ($(this).is(':visible'))
        {
            $('#opc_account_saved').fadeOut('slow');
            $('#submitAccount').show();
        }
    });

});

function bindInputs()
{
    // Order message update
    $('#message').blur(function () {
        $('#opc_delivery_methods-overlay').fadeIn('slow');
        $.ajax({
            type: 'POST',
            url: orderOpcUrl,
            async: false,
            cache: false,
            dataType: "json",
            data: 'ajax=true&method=updateMessage&message=' + encodeURIComponent($('#message').val()) + '&token=' + static_token,
            success: function (jsonData)
            {
                if (jsonData.hasError)
                {
                    var errors = '';
                    for (error in jsonData.errors)
                        //IE6 bug fix
                        if (error != 'indexOf')
                            errors += jsonData.errors[error] + "\n";
                    alert(errors);
                } else
                    $('#opc_delivery_methods-overlay').fadeOut('slow');
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (textStatus != 'abort')
                    alert("TECHNICAL ERROR: unable to save message \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
                $('#opc_delivery_methods-overlay').fadeOut('slow');
            }
        });
    });

    // Recyclable checkbox
    $('input#recyclable').click(function () {
        updateCarrierSelectionAndGift();
    });

    // Gift checkbox update
    $('input#gift').click(function () {
        if ($('input#gift').is(':checked'))
            $('p#gift_div').show();
        else
            $('p#gift_div').hide();
        updateCarrierSelectionAndGift();
    });

    if ($('input#gift').is(':checked'))
        $('p#gift_div').show();
    else
        $('p#gift_div').hide();

    // Gift message update
    $('textarea#gift_message').change(function () {
        updateCarrierSelectionAndGift();
    });

    // Term Of Service (TOS)
    $('#cgv').click(function ()
    {
        var checked = 0;

        if ($('#cgv:checked').length != 0) {
            $("#cgv_msg").hide();
            checked = 1;
        }

        $('#opc_payment_methods-overlay').fadeIn('slow');
        $.ajax({
            type: 'POST',
            url: orderOpcUrl,
            async: true,
            cache: false,
            dataType: "json",
            data: 'ajax=true&method=updateTOSStatusAndGetPayments&checked=' + checked + '&token=' + static_token,
            success: function (json)
            {
                if (json.hasError) {
                    alert(json.errors.toString());
                } else {
                    updatePaymentMethods(json);

                    $('#opc_payment_methods-overlay').fadeOut('slow');
                }
            }
        });
    });
}

function multishippingMode(it)
{
    if ($(it).prop('checked'))
    {
        $('#address_delivery, .address_delivery').hide();
        $('#address_invoice').removeClass('alternate_item').addClass('item');
        $('#multishipping_mode_box').addClass('on');
        $('.addressesAreEquals').hide();
        $('#address_invoice_form').show();

        $('#link_multishipping_form').click(function () {
            return false;
        });
        $('.address_add a').attr('href', addressMultishippingUrl);

        $('#link_multishipping_form').fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'type': 'ajax',
            'onClosed': function ()
            {
                // Reload the cart
                $.ajax({
                    url: orderOpcUrl,
                    data: 'ajax=true&method=cartReload',
                    dataType: 'html',
                    cache: false,
                    success: function (data) {
                        $('#cart_summary').replaceWith($(data).find('#cart_summary'));
                        $('.cart_quantity_input').typeWatch({highlight: true, wait: 600, captureLength: 0, callback: function (val) {
                                updateQty(val, true, this.el)
                            }});
                    }
                });
                updateCarrierSelectionAndGift();
            },
            'onStart': function ()
            {
                // Removing all ids on the cart to avoid conflic with the new one on the fancybox
                // This action could "break" the cart design, if css rules use ids of the cart
                $.each($('#cart_summary *'), function (it, el) {
                    $(el).attr('id', '');
                });
            },
            'onComplete': function ()
            {
                $('#fancybox-content .cart_quantity_input').typeWatch({highlight: true, wait: 600, captureLength: 0, callback: function (val) {
                        updateQty(val, false, this.el)
                    }});
                cleanSelectAddressDelivery();
                $('#fancybox-content').append($('<div class="multishipping_close_container"><a id="multishipping-close" class="button_large" href="#">' + CloseTxt + '</a></div>'));
                $('#multishipping-close').click(function () {
                    var newTotalQty = 0;
                    $('#fancybox-content .cart_quantity_input').each(function () {
                        newTotalQty += parseInt($(this).val());
                    });
                    if (newTotalQty != totalQty) {
                        if (!confirm(QtyChanged)) {
                            return false;
                        }
                    }
                    $.fancybox.close();
                    return false;
                });
                totalQty = 0;
                $('#fancybox-content .cart_quantity_input').each(function () {
                    totalQty += parseInt($(this).val());
                });
            }
        });
    } else
    {
        $('#address_delivery, .address_delivery').show();
        $('#address_invoice').removeClass('item').addClass('alternate_item');
        $('#multishipping_mode_box').removeClass('on');
        $('.addressesAreEquals').show();
        if ($('.addressesAreEquals').find('input:checked').length) {
            $('#address_invoice_form').hide();
        } else {
            $('#address_invoice_form').show();
        }
        $('.address_add a').attr('href', addressUrl);

        // Disable multi address shipping
        $.ajax({
            url: orderOpcUrl,
            async: true,
            cache: false,
            data: 'ajax=true&method=noMultiAddressDelivery'
        });

        // Reload the cart
        $.ajax({
            url: orderOpcUrl,
            async: true,
            cache: false,
            data: 'ajax=true&method=cartReload',
            dataType: 'html',
            success: function (data) {
                $('#cart_summary').replaceWith($(data).find('#cart_summary'));
            }
        });
    }
}

$(document).ready(function () {
    // If the multishipping mode is off assure us the checkbox "I want to specify a delivery address for each products I order." is unchecked.
    $('#multishipping_mode_checkbox').attr('checked', false);
    // If the multishipping mode is on, check the box "I want to specify a delivery address for each products I order.".
    if (typeof (multishipping_mode) != 'undefined' && multishipping_mode) {
        $('#multishipping_mode_checkbox').click();
        $('.addressesAreEquals').hide();
        $('.addressesAreEquals').find('input').attr('checked', false);
    }

    if (typeof (open_multishipping_fancybox) != 'undefined' && open_multishipping_fancybox)
        $('#link_multishipping_form').click();
});



/*<!-- ########################################################################################## -->*/

var opcAnalytics = false;
var opcIsLoaded = false;
var opcStepCurrent;

var opcPageName = 'commande-rapide';

var opcStepName = {
    1: 'panier',
    2: 'identification',
    3: 'adresse',
    4: 'livraison',
    5: 'paiement'
};

var opcStepRegExp = {
    1: new RegExp(opcStepName[1], 'gi'),
    2: new RegExp(opcStepName[2], 'gi'),
    3: new RegExp(opcStepName[3] + "|step=1", 'gi'),
    4: new RegExp(opcStepName[4] + "|step=2", 'gi'),
    5: new RegExp(opcStepName[5] + "|step=3|isPaymentStep", 'gi'),
};

var opcStepSection = {
    1: ['#order-opc_summary', '#order-opc_cart', '#order-opc_carrier'],
    2: ['#order-opc_customer-type', '#order-opc_signup_email'],
    3: ['#order-opc_signup_address', '#order-opc_address', '#order-opc_carrier'],
    4: ['#order-opc_signup_address', '#order-opc_address', '#order-opc_carrier'],
    5: ['#order-opc_cgv', '#order-opc_payment']
};


/* ############################################################################################################################################### */

// Écouteur du changement d'étape via la barre d'adresse du navigateur
var storedHash = window.location.hash;
window.setInterval(function () {
    if (window.location.hash != storedHash) {
        storedHash = window.location.hash;
        opcLoadQueryString();
    }
}, 100);


// Initialisation de la Page 'One Page Check'
$(document).ready(opcInit);


/* ############################################################################################################################################### */

/**
 * Initialiser la Page
 */
function opcInit()
{
    opcCarrierListCountryInit();

    if (opcAnalytics)
        opcGoogleAnalyticsEvent();

    opcLoadQueryString();

    cc_updateDisplay();

    opcBind();
}

/**
 * Construire les écouteurs de la Page
 */
function opcBind()
{
    // Bouton Authentification Type ***
    $("#order-opc_btn_new_customer").click(function () {
        opcStepIdentificationType(0);
    });

    $("#order-opc_btn_old_customer").click(function () {
        opcStepIdentificationType(1);
    });


    // Message Notice des Champs de Saisie ***
    $(".fpv_content .msg.notice").hide();

    $(document).on('focus', ".fpv_content input, .fpv_content textarea", function () {
        if (this.id == 'phone' || this.id == 'phone_mobile')
            $(".fpv_content .infos-phone .msg.notice").stop(true).slideDown();
        else if (this.id == 'phone_invoice' || this.id == 'phone_mobile_invoice')
            $(".fpv_content .infos-phone_invoice .msg.notice").stop(true).slideDown();
        else
            $(this).parent().find(".msg.notice").stop(true).slideDown();
    });

    $(document).on('blur', ".fpv_content input, .fpv_content textarea", function () {
        if (this.id == 'phone' || this.id == 'phone_mobile')
            $(".fpv_content .infos-phone .msg.notice").stop(true).slideUp();
        else if (this.id == 'phone_invoice' || this.id == 'phone_mobile_invoice')
            $(".fpv_content .infos-phone_invoice .msg.notice").stop(true).slideUp();
        else
            $(this).parent().find(".msg.notice").stop(true).slideUp();
    });


    // Validation des Champs de Saisie ***
    // FOCUS : Entrée de Champ à Valider
    $(document).on('focus', ".opc-validate", function () {
        if ((this.id == 'phone' || this.id == 'phone_mobile') && $(this).hasClass('phone_required')) {
            $("#phone, #phone_mobile").removeClass('phone_required');
            opcWarningRemove($("#phone"));
            opcWarningRemove($("#phone_mobile"));
        } else if ((this.id == 'phone_invoice' || this.id == 'phone_mobile_invoice') && $(this).hasClass('phone_required')) {
            $("#phone_invoice, #phone_mobile_invoice").removeClass('phone_required');
            opcWarningRemove($("#phone_invoice"));
            opcWarningRemove($("#phone_mobile_invoice"));
        } else
            opcWarningRemove($(this));
    });
    // BLUR : Sortie de Champ à Valider
    $(document).on('blur', ".opc-validate", function () {
        opcValidate($(this));
    });


    // Bouton Bandeau ***
    $("#order_step_1").click(function () {
        if (opcStepCurrent > 1)
            opcStep(1);
    });
    $("#order_step_2").click(function () {
        if (opcStepCurrent > 2)
            opcStep(2);
    });
    $("#order_step_3").click(function () {
        if (opcStepCurrent > 3)
            opcStep(3);
    });
    $("#order_step_4").click(function () {
        if (opcStepCurrent > 4)
            opcStep(4);
    });
    $("#order_step_5").click(function () {
        if (opcStepCurrent > 5)
            opcStep(5);
    });


    // Bouton Validation eMail Nouveau Client ***
    $("#order-opc_signup_email_validation").on('click', function () {
        opcEmailVerif();
    });

    // KEYDOWN : Changement de contenu du champ "First Step Adresse Email"
    $("#email").keydown(function (event) {
        if (event.which == 13) {
            opcEmailVerif();
            return false;
        }
    });

    // Bouton Login Client ***
    $("#SubmitLogin").on('click', function () {
        $("#order-opc_login_warning_email_exist").hide();
    });

    // Bouton LostPassword ***
    $("#SubmitLostPassword").on('click', function () {
        opcEmailPassword();
    });

    // Soumission du Formulaire Nouvelle Adresse / Nouveau Client ***
    /*$("#new_account_form").on('submit', function() {
     return opcValidateAddress();
     });*/


    // Ajout d'une Adresse ***
    $(document).on('click', ".addresses .address_add span", function ()
    {
        var type = ($(this).parent().hasClass('address_add_invoice')) ? 'invoice' : 'delivery';

        $.ajax({
            url: orderOpcUrl,
            async: true,
            cache: false,
            data: 'method=opcAddressEdit',
            dataType: 'html',
            success: function (data) {
                opcAddressEditDisplay(data, type);
            }
        });
    });

    // Mise à jour d'une Adresse ***
    $(document).on('click', ".address .address_update span", function ()
    {
        var id = parseInt(this.dataset.id_address);
        var type = ($(this).parents(".address").attr('id') == "address_invoice") ? 'invoice' : 'delivery';

        $.ajax({
            url: orderOpcUrl,
            async: true,
            cache: false,
            data: 'method=opcAddressEdit&id_address=' + id,
            dataType: 'html',
            success: function (data) {
                opcAddressEditDisplay(data, type);
            }
        });
    });

    // Enregistrement de l'édition d'une adresse ***
    $(document).on('click', "#submitAccount", function (evt)
    {
        if ($(this).parents("#opc_address_edit").length == 1) {

            if (!opcValidateAddress()) {
                evt.stopPropagation();
                evt.preventDefault();
                return false;
            }

            opcAddressEditSave();
        }
    });

    // Retour à l'étape précédente 'Adresse' par un clic sur la zone grisé 'Adresse' pendant l'étape 'Livraison' ***
    $(document).on('click', "#order-opc_address", function (evt) {
        if (opcStepCurrent == 4 && evt.target.id == "order-opc_address")
            opcStep(3);
    });

    // Validation de l'étape Livraison ***
    $(document).on('click', "#order-opc_carrier_submit", function () {
        $o = $(".delivery_option_radio:checked").parents(".delivery_option");
//        if (fpv_isCarrierDPD($o)) {
//            fpv_closePopUpDPD();
//            $(".delivery_option_radio:checked").trigger('change');
//        } else

        $(".delivery_option_radio:checked").trigger('change');
        var checked = 0;
        
        if ($('#cgv:checked').length != 0) {
            $("#cgv_msg").hide();
            checked = 1;
        }

        $('#opc_payment_methods-overlay').fadeIn('slow');
        $.ajax({
            type: 'POST',
            url: orderOpcUrl,
            async: true,
            cache: false,
            dataType: "json",
            data: 'ajax=true&method=updateTOSStatusAndGetPayments&checked=' + checked + '&token=' + static_token,
            success: function (json)
            {
                if (json.hasError) {
                    alert(json.errors.toString());
                } else {
                    updatePaymentMethods(json);

                    $('#opc_payment_methods-overlay').fadeOut('slow');
                }
            }
        });
        opcStep(5);
    });

    // FIXME: Suppression du HASH de URL par Safari lors d'une réponse HTTP/3xx
    // CLICK : Sélection d'un point Relais Pickup ***
    $(document).on('click', "input[name='dpdfrance_relay_id_opc'], #dpdfrance_predict_gsm_button_opc", function () {
        var sKey = "dpdfrance_has_click";
        var sValue = "has_dpd_click";
        var sExpires = "; max-age=60"; 	// Durée de vie de 1 minute du cookie
        var sDomain = location.hostname;	// Restriction au domaine courant
        var sPath = location.pathname;		// Restriction au répertoire de commande
        var bSecure = location.protocol == "https:" ? true : false;

        document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    });

    // Vérification des CGV ***
    $(document).on('click', "#HOOK_PAYMENT.preview_list", function () {
        $("#cgv_msg").show();

        if ($(document).scrollTop() > $("#cgv").offset().top)
            $('html, body').animate({
                scrollTop: $("#order-opc_cgv").offset().top + "px"
            }, 600);
    });
}


/* ############################################################################################################################################### */

/**
 * Définir la Page_Step à afficher par rapport à l'URL de la barre d'Adresse
 */
function opcLoadQueryString()
{
    if (window.location.hash != "#" + opcStepName[3] + "_edit")
        $("#order-opc_address").removeClass('in-address-edit');

    if (window.location.href.search(opcStepRegExp[5]) > -1) {
        opcStepShow(5);
    } else if (window.location.href.search(opcStepRegExp[4]) > -1) {
        opcStepShow(4);
    } else if (window.location.href.search(opcStepRegExp[3]) > -1) {
        if (!opcStepIdentificationOk())
            opcStep(2, false);
        else
            opcStepShow(3);
    } else if (window.location.href.search(opcStepRegExp[2]) > -1) {
        if (isLogged)
            opcStep(3, false);
        else
            opcStepShow(2);
    } else {

        // FIXME: Suppression du HASH de URL par Safari lors d'une réponse HTTP/3xx
        var sKey = "dpdfrance_has_click";
        var sValue = decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        if (sValue == "has_dpd_click")
            window.location.replace("#" + opcStepName[5]);
        else
            opcStepShow(1);
    }
    opcIsLoaded = true;
}


/**
 * Changer l'URL avec la Page_Step désirée
 */
function opcStep(step, history, force)
{
    var hash = null;
    if (typeof history != "boolean")
        history = true;

    if (typeof force != "boolean")
        force = false;

    switch (step) {
        case 1 : // Récapitulatif
            hash = "#" + opcStepName[1];
            break;

        case 2 : // Identification
            if (opcStepCurrent == 3)
                opcStep(1, false);
            else if (isLogged)
                opcStep(3, false);
            else
                hash = "#" + opcStepName[2];
            break;

        case 3 : // Adresse
            if (!opcStepIdentificationOk())
                opcStep(2);
            else
                hash = "#" + opcStepName[3];
            break;

        case 4 : // Livraison
            if (!isLogged)
                opcStep(2, false);
            else if (!opcDeliveryAddress())
                opcStep(3);
            else
                hash = "#" + opcStepName[4];
            break;

        case 5 : // Paiement
            if (!isLogged)
                opcStep(2, false);
            else
                hash = "#" + opcStepName[5];
            break;
    }
    if (hash != null) {
        if (history)
            window.location.assign(hash);
        else
            window.location.replace(hash);
    }
}


/**
 * Afficher une Page_Step
 * 
 * @param step integer  "Numéro Index de l'étape à afficher"
 */
function opcStepShow(step)
{
    if (step === opcStepCurrent)
        return;
    opcStepCurrent = step;

    // Mise à jour de la scène de la colonne centrale du Tunnel
    $("#center_column").removeClass('opc-step_1 opc-step_2 opc-step_3 opc-step_4 opc-step_5').addClass('opc-step_' + step);

    // Initialisation de l'étape Panier
    if (step == 1)
        fpv_closePopUpDPD(true);

    // Initialisation de l'étape Adresse
    if (step == 3)
        $("#order-opc_address").removeClass('in-address-edit');

    // Initialisation de l'étape Livraison
    if (step == 4)
        fpv_closePopUpDPD(true);

    // Initialisation du récapitulation de la commande de l'étape de Paiement
    if (step == 5)
        opcPaymentRefresh();

    // Envoi des informations à Google Analytics
    if (opcAnalytics)
        opcGoogleAnalytics();

    // Mise à jour du Bandeau des étapes
    opcBandeau();

    // Masque la zone d'affichage des erreurs du formulaire de création de compte
    $("#opc_account_errors").hide();
}


/* ############################################################################################################################################### */

/**
 * Vérifier si l'étape d'identification est OK
 */
function opcStepIdentificationOk()
{
    if (isLogged)
        return true;

    if ($.trim($("#email").val()) != "")
        return true;

    return false;
}

/**
 * Changer l'affichage par rapport au type d'identification
 * 
 * @param type integer  "Type d'Identification (0=new_customer / 1=old_customer")
 */
function opcStepIdentificationType(type)
{
    if (type) {  // old_customer
        $("#center_column").addClass('old_customer');

    } else {  // new_customer
        $("#center_column").removeClass('old_customer');

    }
}


/* ############################################################################################################################################### */

/**
 * Lancer la vérification de l'existance l'adresse email saisie
 */
function opcEmailVerif() {
    var $o = $("#email");
    var email = $o.val();

    opcWarningRemove();

    if (!opcEmailValidate(email)) {
        $o.focus();
        opcWarningShow($o, "Veuillez vérifier la syntaxe de votre adresse email.");
        return false;
    }

    $.ajax({
        url: orderOpcUrl,
        data: {
            'method': 'opcEmail',
            'ajax': 1,
            'email': email
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            if (data.opcEmail) {
                $("#order-opc_login_warning_email_exist").show();
                $("#login_email").val(email);
                $("#login_passwd").val('');
                opcStepIdentificationType(1);
                $("#login_passwd").focus();
            } else
                opcStep(3);
        }
    });
}

/**
 * Lancer la procédure de récupération de mot de passe
 */
function opcEmailPassword() {
    var $o = $("#login_email");
    var email = $o.val();

    opcWarningRemove();

    if ($.trim(email) == "") {
        $o.focus();
        opcWarningShow($o, txtValidateRequired);
        return false;
    }

    if (!opcEmailValidate(email)) {
        $o.focus();
        opcWarningShow($o, txtValidateEmailSyntax);
        return false;
    }

    $.ajax({
        url: orderOpcUrl,
        data: {
            'method': 'opcPassword',
            'email': email
        },
        dataType: 'json',
        cache: false,
        success: function (data) {
            if (data.hasError) {
                $("#opc_login_errors").html(data.errors).show();

            } else {
                $("#order-opc_login_password_send").show();
                $("#login_passwd").focus();
            }
        }
    });
}


/**
 * Valider la syntaxe d'une adresse eMail
 * 
 * @param email string  "Adresse eMail à vérifier"
 * @returns boolean  "Syntaxe Valide"
 */
function opcEmailValidate(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
;


/* ############################################################################################################################################### */

/**
 * Récupérer l'adresse de Livraison sélectionnée
 * 
 * @returns Id de Adresse de Livraison sélectionnée
 */
function opcDeliveryAddress()
{
    var id_address = 0;

    if (typeof deliveryAddress != "undefined" && parseInt(deliveryAddress) > 0)
        id_address = parseInt(deliveryAddress);

    else {
        var id_address_form = $("#opc_id_address_delivery, #id_address_delivery").val();
        if (typeof id_address_form != "undefined" && !isNaN(parseInt(id_address_form)) && parseInt(id_address_form) > 0)
            id_address = parseInt(id_address_form);
    }

    deliveryAddress = id_address;
    return deliveryAddress;
}

/**
 * Afficher la page d'édition d'adresse
 * 
 * @param data XHR  Réponse de la Réquête AJAX
 */
function opcAddressEditDisplay(data, type)
{
    var $form = $(data).find('#order-opc_signup_address');

    $form.find('#order-opc_signup_email').remove();
    $form.find('#opc_id_customer').remove();
    $form.find('#opc_id_address_delivery').remove();
    $form.find('#opc_id_address_invoice').remove();
    $form.find('#opc_invoice_address').remove();
    $form.find('.checkbox.is_customer_param').remove();

    aliasEdit = {'value': $form.find('#alias').val(), 'type': type};

    var html = $form.html();

    $("#opc_address_edit").html('<div id="opc_account_errors" class="msg warning" style="display:none; width:100%;"></div><div id="opc_new_account" class="' + type + '-address">' + html + '</div>');

    initAutocompleteAddressForm();

    $("#order-opc_address").addClass('in-address-edit');

    window.location.href = "#" + opcStepName[3] + "_edit";
}

/**
 * Enregistrer la page d'édition d'adresse
 * 
 * @returns Réussite de l'enregistrement
 */
function opcAddressEditSave()
{
    var id = parseInt($("#opcAddressEdit_id").val());

    aliasUpdateAddressesSelected = false;

    $("#opc_new_account").addClass('on_load');

    if (saveAddress('edit', id, aliasEdit.type))
    {
        aliasEdit = {'value': null, 'type': null};

        var idInterval = setInterval(function () {
            if (aliasUpdateAddressesSelected) {
                clearInterval(idInterval);
                opcAddressEditRefresh();
                aliasUpdateAddressesSelected = false;
            }
        }, 100);
    } else {
        $("#opc_new_account").removeClass('on_load');
    }
}

/**
 * Enregistrer la page d'édition d'adresse
 */
function opcAddressEditRefresh()
{
    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "json",
        data: 'ajax=true&method=getAddressBlockAndCarriersAndPayments&token=' + static_token,
        success: function (json)
        {
            if (json.order_opc_adress)
                $("#order-opc_address").replaceWith(json.order_opc_adress);

            $.each(json.summary.products, function () {
                updateAddressId(this.id_product, this.id_product_attribute, '0', this.id_address_delivery);
            });

            updateCartSummary(json.summary);
            updateAddressesDisplay(true);
            updateCarrierList(json.carrier_data);
            updatePaymentMethods(json);

            $("#opc_new_account").removeClass('on_load');

            window.location.replace("#" + opcStepName[3]);
        }
    });
}


/* ############################################################################################################################################### */

/**
 * Récupérer la liste des Transporteurs d'un pays
 * 
 * @returns Id du pays à simuler
 */
function opcCarrierListCountry(id_country)
{
    $.ajax({
        type: 'POST',
        url: orderOpcUrl,
        async: true,
        cache: false,
        dataType: "json",
        data: 'method=opcCountryChange&id_country=' + id_country + '&token=' + static_token,
        success: function (json) {
            updateCarrierList(json);
            $(".delivery_option.item.checked input").trigger('change');
        }
    });
}

/**
 * Initialiser les listes déroulantes des pays
 */
function opcCarrierListCountryInit()
{
    if (isLogged) {
        $("#BLOCK_SHIPPING_CELL").addClass('no-country-simulate')
        id_country = parseInt($('#id_country').val());
    } else {
        if (id_country != 0)
            $('#id_country option[value="' + id_country + '"]').prop('selected', true);

        $('#id_country_simulate option[value="' + $("#id_country").val() + '"]').prop('selected', true);
    }
}


/* ############################################################################################################################################### */

/**
 * Initialiser l'étape 5 / Paiement
 */
function opcPaymentRefresh()
{
    $("#cgv_msg").hide();

//	if($("#HOOK_PAYMENT .payment_module").length == 0) {
    if ($('#cgv:checked').length != 0) {
        $("#HOOK_PAYMENT")
                .removeClass('preview_list');
    } else {
        $("#HOOK_PAYMENT")
                .html($("#HOOK_PAYMENT_PREVIEW").html())
                .addClass('preview_list');
    }
}


/* ############################################################################################################################################### */

/**
 * Valider la Syntaxe du contenu du Champ de Saisie
 */
function opcValidate($input)
{
    opcWarningRemove($input);

    if (!$input.hasClass('opc-validate'))
        return true;

    if ($.trim($input.val()) == "") {
        if ($input.hasClass('validate_required')) {
            opcWarningShow($input, txtValidateRequired);
            return false;
        } else
            return true;
    }

    if ($input.hasClass('validate_email') && !opcEmailValidate($input.val())) {
        opcWarningShow($input, txtValidateEmailSyntax);
        return false;
    }

    if ($input.hasClass('validate_name') && !validate_isName($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }

    if ($input.hasClass('validate_generic-name') && !validate_isGenericName($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }

    if ($input.hasClass('validate_address') && !validate_isAddress($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }

    if ($input.hasClass('validate_postcode') && !validate_isPostCode($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }

    if ($input.hasClass('validate_city') && !validate_isCityName($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }

    if ($input.hasClass('validate_message') && !validate_isMessage($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }

    if ($input.hasClass('validate_phone') && !validate_isPhoneNumber($input.val())) {
        opcWarningShow($input, txtValidateSyntax);
        return false;
    }


    return true;
}


/* ############################################################################################################################################### */

/**
 * Envoyer l'URL à Google Analytics
 */
function opcGoogleAnalytics(hash)
{
    if (typeof hash == "undefined")
        hash = location.hash

    hash = hash.replace("#", "/");

    if (opcAnalytics && typeof ga != "undefined")
        ga('send', 'pageview', '/' + opcPageName + hash);
}


/**
 * Instanciation des événements pour l'écoute Google Analytics
 */
function opcGoogleAnalyticsEvent()
{
    $(document).on("click", "#order-opc_payment .payment_module", function () {

        if ($(this).hasClass('atos')) // Choix de CB / Atos
            opcGoogleAnalytics("#" + opcStepName[5] + "_cb_atos");

        else if ($(this).find("img[src*='paypal/']").length > 0) // Choix de Paypal
            opcGoogleAnalytics("#" + opcStepName[5] + "_paypal");

    });
}


/* ############################################################################################################################################### */

/**
 * Mettre à jour le Bandeau des étapes
 */
function opcBandeau()
{
    $("#order_step li").removeClass('step_todo step_current step_done');
    $("#ass_global").hide();

    switch (opcStepCurrent) {
        case 1 : // Panier
            $("#order_step_1").addClass('step_current');
            $("#order_step_2").addClass('step_todo');
            $("#order_step_3").addClass('step_todo');
            $("#order_step_4").addClass('step_todo');
            $("#order_step_5").addClass('step_todo');
            $("#ass_global").show();
            break;

        case 2 : // Identification
            $("#order_step_1").addClass('step_done');
            $("#order_step_2").addClass('step_current');
            $("#order_step_3").addClass('step_todo');
            $("#order_step_4").addClass('step_todo');
            $("#order_step_5").addClass('step_todo');
            break;

        case 3 : // Adresse
            $("#order_step_1").addClass('step_done');
            $("#order_step_2").addClass('step_done');
            $("#order_step_3").addClass('step_current');
            $("#order_step_4").addClass('step_todo');
            $("#order_step_5").addClass('step_todo');
            break;

        case 4 : // Livraison
            $("#order_step_1").addClass('step_done');
            $("#order_step_2").addClass('step_done');
            $("#order_step_3").addClass('step_done');
            $("#order_step_4").addClass('step_current');
            $("#order_step_5").addClass('step_todo');
            break;

        case 5 : // Paiement
            $("#order_step_1").addClass('step_done');
            $("#order_step_2").addClass('step_done');
            $("#order_step_3").addClass('step_done');
            $("#order_step_4").addClass('step_done');
            $("#order_step_5").addClass('step_current');
            break;
    }
}


/* ############################################################################################################################################### */

/**
 * Afficher un message avertissement sous un champ
 * 
 * @param $o jQuery  "Champ concerné"
 * @param msg string  "Message d'avertissement"
 */
function opcWarningShow($o, msg)
{
    $o.parent().find(".opc-warning").remove();

    var warn = $("<div>");
    warn.addClass("opc-warning");
    warn.html(msg);
    if ($o.parent().find(".sup_suffixe").length)
        warn.insertAfter($o.parent().find(".sup_suffixe"));
    else
        warn.insertAfter($o);

    $o.addClass("opc-warning-input");
}

/**
 * Détruire tous les messages d'avertissement
 */
function opcWarningRemove($input)
{
    if ($input) {
        $input.parent().find(".opc-warning").remove();
        $input.parent().find(".opc-warning-input").removeClass("opc-warning-input");

    } else {
        $(".opc-warning").remove();
        $(".opc-warning-input").removeClass("opc-warning-input");

        $("#order-opc_login_warning_email_exist").hide();
        $("#order-opc_login_password_send").hide();

        $("#opc_login_errors").hide();
    }
}


/**
 * Afficher un message avertissement sous un champ
 * 
 * @param $o jQuery  "Champ concerné"
 */
function opcValidateShow($o)
{
    $o.parent().find(".opc-warning").remove();
    $o.parent().find(".opc-validate").remove();

    $o.addClass("opc-validate-input");
}

/**
 * Détruire tous les messages d'avertissement
 * 
 * @param $input jQuery  "Champ concerné"
 */
function opcValidateRemove($input)
{
    $input.parent().find(".opc-validate").remove();
    $input.parent().find(".opc-validate-input").removeClass("opc-validate-input");
}

/**
 * Valider le formulaire Nouvelle Adresse
 */
function opcValidateAddress()
{
    var bOk = true;

    var fnFalse = function () {
        $("#new_account_form_no_valid").show();
        if (bOk)
            bOk = false;
    };

    $("#new_account_form_no_valid").hide();

    if (!opcValidate($("#customer_lastname")))
        fnFalse();
    if (!opcValidate($("#customer_firstname")))
        fnFalse();

    if (!opcValidate($("#address1")))
        fnFalse();
    if (!opcValidate($("#postcode")))
        fnFalse();
    if (!opcValidate($("#city")))
        fnFalse();

    if (bOk && $.trim($("#alias").val()) == "")
        $("#alias").val(($("#address1").val() + ", " + $("#city").val()).substring(0, 32));
    if (!opcValidate($("#alias")))
        fnFalse();

    if (!opcValidate($("#phone")))
        fnFalse();
    if (!opcValidate($("#phone_mobile")))
        fnFalse();
    if ($.trim($("#phone").val()) == "" && $.trim($("#phone_mobile").val()) == "") {
        $("#phone, #phone_mobile").addClass('phone_required');
        opcWarningShow($("#phone"), txtValidateNumberPhone);
        opcWarningShow($("#phone_mobile"), "");
        fnFalse();
    }

    if ($("#invoice_address").prop('checked')) {

        if (!opcValidate($("#lastname_invoice")))
            fnFalse();
        if (!opcValidate($("#firstname_invoice")))
            fnFalse();

        if (!opcValidate($("#address1_invoice")))
            fnFalse();
        if (!opcValidate($("#postcode_invoice")))
            fnFalse();
        if (!opcValidate($("#city_invoice")))
            fnFalse();

        if (bOk && $.trim($("#alias_invoice").val()) == "")
            $("#alias_invoice").val(($("#address1_invoice").val() + ", " + $("#city_invoice").val()).substring(0, 32));
        if (!opcValidate($("#alias_invoice")))
            fnFalse();

        if (!opcValidate($("#phone_invoice")))
            fnFalse();
        if (!opcValidate($("#phone_mobile_invoice")))
            fnFalse();
        if ($.trim($("#phone_invoice").val()) == "" && $.trim($("#phone_mobile_invoice").val()) == "") {
            $("#phone_invoice, #phone_mobile_invoice").addClass('phone_required');
            opcWarningShow($("#phone_invoice"), txtValidateNumberPhone);
            opcWarningShow($("#phone_mobile_invoice"), "");
            fnFalse();
        }
    }

    return bOk;
}


/* ############################################################################################################################################### */

function cc_updateDisplay()
{
    cc_removeMsg()
    cc_displayDimexact();
    cc_displayFreeShip();
}


/* ############################################################################################################################################### */
/* -- JavaScript AutoCompletion Google Place -- */

$(document).ready(function () {
    $('#alias')
//	 .on('focus', geolocate)
            .on('keydown', function (e) {

                $("#account-creation_form .account_creation .fpv_content > p.alias_show").show();

                if (e.which == 13) {
                    $(this).blur();
                    e.preventDefault();
                    return false;
                }
            });

    $('#alias_invoice').on('keydown', function (e) {
        if (e.which == 13) {
            $(this).blur();
            e.preventDefault();
            return false;
        }
    });

    setTimeout(function () {
        if (!autocompleteIsInit && !isLogged)
            initAutocompleteAddressForm();
    }, 1000);
});

var autocompleteIsInit = false;
var placeSearch, autocomplete;


function initAutocompleteAddressForm()
{
    if (document.getElementById('alias') !== null) {
        autocomplete = new google.maps.places.Autocomplete(document.getElementById('alias'), {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
    }

    if (document.getElementById('alias_invoice') !== null) {
        autocomplete_invoice = new google.maps.places.Autocomplete(document.getElementById('alias_invoice'), {types: ['geocode']});
        autocomplete_invoice.addListener('place_changed', fillInAddress_invoice);
    }

    autocompleteIsInit = true;
}

/** **** * ** *** * ** ** ** *** * *** ** ** * * *** **/

var componentForm = {
    street_number: {type: 'short_name', id: 'address1'},
    route: {type: 'long_name', id: 'address1'},
    sublocality_level_1: {type: 'long_name', id: 'address1'},
    locality: {type: 'long_name', id: 'city'},
    country: {type: 'long_name', id: 'id_country', fn: selectCountry},
    postal_code: {type: 'short_name', id: 'postcode'}
};

function fillInAddress()
{
    var place = autocomplete.getPlace();

    for (var component in componentForm) {
        document.getElementById(componentForm[component].id).value = '';
        document.getElementById(componentForm[component].id).disabled = false;
    }

    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType].type];

            if (componentForm[addressType].fn && typeof componentForm[addressType].fn == 'function')
                componentForm[addressType].fn.call(this, val);
            else {
                if (addressType == 'street_number')
                    val += " ";

                document.getElementById(componentForm[addressType].id).value += val;
            }
        }
    }

    opcWarningRemove($("#address1"));
    opcWarningRemove($("#postcode"));
    opcWarningRemove($("#city"));
}

function geolocate()
{
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}

function selectCountry(txt_country)
{
    var id_country;
    var regex = new RegExp(txt_country, 'gi');

    $("#id_country option").each(function () {
        if (regex.test(this.text)) {
            id_country = this.value;
            $(this).prop('selected', true);
        }
    });

    return id_country;
}

/** **** * ** *** * ** ** ** *** * *** ** ** * * *** **/


var componentForm_invoice = {
    street_number: {type: 'short_name', id: 'address1_invoice'},
    route: {type: 'long_name', id: 'address1_invoice'},
    sublocality_level_1: {type: 'long_name', id: 'address1_invoice'},
    locality: {type: 'long_name', id: 'city_invoice'},
    country: {type: 'long_name', id: 'id_country_invoice', fn: selectCountry_invoice},
    postal_code: {type: 'short_name', id: 'postcode_invoice'}
};

function fillInAddress_invoice()
{
    var place = autocomplete_invoice.getPlace();

    for (var component in componentForm_invoice) {
        document.getElementById(componentForm_invoice[component].id).value = '';
        document.getElementById(componentForm_invoice[component].id).disabled = false;
    }

    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (componentForm_invoice[addressType]) {
            var val = place.address_components[i][componentForm_invoice[addressType].type];

            if (componentForm_invoice[addressType].fn && typeof componentForm_invoice[addressType].fn == 'function')
                componentForm_invoice[addressType].fn.call(this, val);
            else {
                if (addressType == 'street_number')
                    val += " ";

                document.getElementById(componentForm_invoice[addressType].id).value += val;
            }
        }
    }

    opcWarningRemove($("#address1_invoice"));
    opcWarningRemove($("#postcode_invoice"));
    opcWarningRemove($("#city_invoice"));
}

function geolocate_invoice()
{
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete_invoice.setBounds(circle.getBounds());
        });
    }
}

function selectCountry_invoice(txt_country)
{
    var id_country_invoice;
    var regex = new RegExp(txt_country, 'gi');

    $("#id_country_invoice option").each(function () {
        if (regex.test(this.text)) {
            id_country_invoice = this.value;
            $(this).prop('selected', true);
        }
    });

    return id_country_invoice;
}


/* ############################################################################################################################################### */

function resizeAddressesBox(nameBox)
{
    maxHeight = 0;

    if (typeof (nameBox) == 'undefined')
        nameBox = '.address';
    $(nameBox).each(function ()
    {
        $(this).css('height', 'auto');
        currentHeight = $(this).height();
        if (maxHeight < currentHeight)
            maxHeight = currentHeight;
    });
    $(nameBox).height(maxHeight);
}


/* ############################################################################################################################################### */

var aliasUpdateAddressesSelected = false;

var aliasEdit = {'value': null, 'type': null};

var aliasInitial = {'delivery': null, 'invoice': null};


/**
 * Récupérer l'"Alias" non-doublonné
 * 
 * @param alias  "'Alias' initial"
 * @param indexExclude  "Index à exclure lors recherche au sein du tableau des Alias"
 * @return  "Nouvel 'Alias'"
 */
function aliasGet(alias, indexExclude)
{
    alias = alias.substr(0, 32);

    var lenCutAlias = Math.min(30, alias.length);

    for (var i = 2; i < 10; i++) {
        if (!aliasExist(alias, indexExclude)) {
            break;
        }
        alias = alias.substr(0, lenCutAlias) + "_" + i;
    }

    return alias;
}

/**
 * Déterminer si l'"Alias" existe déjà
 * 
 * @param alias  "Alias a vérifié"
 * @param indexExclude  "Index à exclure lors recherche au sein du tableau des Alias"
 * @return
 */
function aliasExist(alias, indexExclude)
{
    if (typeof indexExclude == 'undefined')
        indexExclude = -1;

    if ($.type(aliasAddressList) == 'array') {
        for (var i = 0; i < aliasAddressList.length; i++) {
            if (i == indexExclude)
                continue;
            if (alias == aliasAddressList[i])
                return true;
        }
        return false;
    } else
        return true;
}

/**
 * Récupérer l'index de l'"Alias" au sein du tableau des Alias `aliasAddressList´
 * 
 * @param alias  "'Alias' recherché"
 * @return  "Index au sein du tableau des Alias (`-1´ SI non trouvé)"
 */
function aliasIndex(alias)
{
    var index = -1;

    for (var i = 0; i < aliasAddressList.length; i++) {
        if (alias == aliasAddressList[i]) {
            index = i;
            break;
        }
    }

    return index;
}

/**
 * Récupérer & Mémoriser la valeur d'un champ "Alias"
 * 
 * @param hasInvoice  "'Alias' de facturation"
 * @return  "Nouvelle valeur du champ 'Alias'"
 */
function aliasGetInputVal(hasInvoice)
{
    var $o = $('#alias' + (hasInvoice ? '_invoice' : ''));
    var str = $o.val();
    var key = hasInvoice ? 'invoice' : 'delivery';
    var obj = {'value': null, 'index': null};

    obj.value = str;

    str = aliasGet(str);
    aliasAddressList.push(str);

    obj.index = aliasIndex(str);

    $o.val(str);
    aliasInitial[key] = obj;

    return str;
}

/**
 * Ré-affectation des valeurs des champs "Alias"
 * 
 * @param noClean  "Pas de nettoyage des valeurs mémorisées des champs 'Alias'"
 */
function aliasReassignInput(noClean)
{
    if (aliasInitial.delivery != null) {
        $('#alias').val(aliasInitial.delivery.value);
        aliasAddressList.splice(aliasInitial.delivery.index, 1);

        if (aliasInitial.invoice != null && aliasInitial.delivery.index < aliasInitial.invoice.index) {
            aliasInitial.invoice.index = Math.max(0, (aliasInitial.invoice.index - 1));
        }
    }

    if (aliasInitial.invoice != null) {
        $('#alias_invoice').val(aliasInitial.invoice.value);
        aliasAddressList.splice(aliasInitial.invoice.index, 1);
    }

    if (!noClean) {
        aliasCleanInitialAssignment();
    }
}

/**
 * Nettoyer les valeurs mémorisées des champs "Alias"
 */
function aliasCleanInitialAssignment()
{
    aliasInitial.delivery = null;
    aliasInitial.invoice = null;
}

//carrier file
var cc_inLoad = true;

$(document).ready(function () {
    $(document).ready(function () {
        $(document).on('change', '#id_country', function () {
            resetAjaxQueries();
            updateStateByIdCountry();
        });

        if (SE_RefreshMethod == 0)
        {
            $(document).on('change', '#id_state', function () {
                resetAjaxQueries();
                updateCarriersList();
            });

            $(document).on('keyup', '#zipcode', function () {
                if (e.keyCode == '13')
                {
                    resetAjaxQueries();
                    updateCarriersList();
                }
            });
        }

        $(document).on('click', '#update_carriers_list', function () {
            updateCarriersList();
        });

        $(document).on('click', '#carriercompare_submit', function () {
            resetAjaxQueries();
            simulateSelection();
            return false;
        });

        $(document).on('change', "input[name='carrier_price_value']", function () {
            disableUpdateCart();
        });

        $(document).on('click', "#shipping_simulate #carriers_list .item", function () {
            cc_set($(this));
            if (typeof opc == 'undefined' || !opc)
                cc_save();
        });

//        updateStateByIdCountry();
        disableUpdateCart();

    });
});


var ajaxQueries = new Array();

function displayWaitingAjax(type, message)
{
    $('#SE_AjaxDisplay').find('p').html(message);
    $('#SE_AjaxDisplay').css('display', type);

    if (type == "block")
        $('#update_carriers_list').attr("disabled", "disabled");
    else if (type == "none")
        $('#update_carriers_list').removeAttr("disabled");

    disableUpdateCart();
}

function disableUpdateCart()
{
    var checked = $('input[name="carrier_price_value"]:checked').val()
    if (typeof checked == "undefined")
        $('#carriercompare_submit').attr("disabled", "disabled");
    else
        $('#carriercompare_submit').removeAttr("disabled");
}

function updateStateByIdCountry()
{
    $('#id_state').children().remove();
    $('#availableCarriers').slideUp('fast');
    $('#states').slideUp('fast');
    displayWaitingAjax('block', SE_RefreshStateTS);

    var query = $.ajax({
        type: 'POST',
        headers: {"cache-control": "no-cache"},
        url: baseDir + 'modules/carriercompare/ajax.php' + '?rand=' + new Date().getTime(),
        data: 'method=getStates&id_country=' + $('#id_country').val() + '&token=' + static_token,
        dataType: 'json',
        success: function (json) {
            if (json.length)
            {
                for (state in json)
                {
                    $('#id_state').append('<option value=\'' + json[state].id_state + '\' ' + (id_state == json[state].id_state ? 'selected="selected"' : '') + '>' + json[state].name + '</option>');
                }
                $('#states').slideDown('fast');
            }
            if (SE_RefreshMethod == 0)
                updateCarriersList();
            displayWaitingAjax('none', '');
        }
    });
    ajaxQueries.push(query);
}

function updateCarriersList(not_set)
{
    $('#carriercompare_errors_list').children().remove();

    $('#availableCarriers').slideUp('normal', function () {
        $(this).find(('tbody')).children().remove();
        $('#noCarrier').slideUp('fast');
        displayWaitingAjax('block', SE_RetrievingInfoTS);

        var query = $.ajax({
            type: 'POST',
            headers: {"cache-control": "no-cache"},
            url: baseDir + 'modules/carriercompare/ajax.php' + '?rand=' + new Date().getTime(),
            data: 'method=getCarriers&id_country=' + $('#id_country').val() + '&id_state=' + $('#id_state').val() + '&zipcode=' + $('#zipcode').val() + '&token=' + static_token,
            dataType: 'json',
            success: function (json) {
                if (json.length)
                {
                    var cc_isChecked = false;

                    var html = '';

                    html += '<tr class="cc-line"><td>';

                    $.each(json, function (index, carrier)
                    {
                        var id = carrier.id_carrier.substr(1, carrier.id_carrier.substr(0, 1));

                        if (id_carrier == id)
                            cc_isChecked = true;

                        html += '<div class="cc-cell">';

                        html += '<div class="item ' + (id_carrier == id ? 'checked' : '') + '" data-id="' + id + '">';

                        html += '<div class="carrier_selection" width="64px">' +
                                '&nbsp; <input type="radio" name="carrier_id" value="' + carrier.id_carrier + '" id="id_carrier' + carrier.id_carrier + '" ' + (id_carrier == carrier.id_carrier ? 'checked="checked"' : '') + '/>' +
                                '</div>';

                        html += '<div class="carrier_name">' +
                                '<label for="id_carrier' + carrier.id_carrier + '">' +
                                (carrier.img ? '<img src="' + carrier.img + '" alt="' + carrier.name + '" />' : carrier.name) +
                                '</label>' +
                                '</div>';

                        html += '<div class="carrier_infos">' +
                                '<div class="cc-name">' + carrier.name + '</div>' +
                                '</div>';

                        html += '<div class="carrier_price">';
                        if (carrier.price) {
                            html += '<span class="price">';
                            if (displayPrice == 1)
                                html += formatCurrency(carrier.price_tax_exc, currencyFormat, currencySign, currencyBlank);
                            else
                                html += formatCurrency(carrier.price, currencyFormat, currencySign, currencyBlank);
                            html += '</span>';
                        } else {
                            html += txtFree;
                        }
                        html += '</div>';

                        if ($.type(dataCarriers[id]) == 'object')
                            html += '<div class="cc-delai"><span class="name">' + dataCarriers[id].prefixe + dataCarriers[id].date + '</span></div>';

                        html += '<div class="carrier_text">' + ((carrier.delay != null) ? carrier.delay : '') + '</div>';

                        html += '</div>'; // END `.item`

                        html += '</div>'; // END `.cc-cell`
                    });

                    html += '</td></tr>';

                    $('#carriers_list').append(html);

                    displayWaitingAjax('none', '');
                    $('#availableCarriers').slideDown(function ()
                    {
                        if (cc_isChecked) {
                            if (opcStepCurrent == 1)
                                cc_setId(id_carrier);
                        } else
                            cc_check();

                        if (typeof opc == 'undefined' || !opc) {
                            if (cc_inLoad)
                                cc_load();
                            else
                                cc_save();
                        }
                    });
                } else
                {
                    displayWaitingAjax('none', '');
                    $('#noCarrier').slideDown();
                }
            }
        });
        ajaxQueries.push(query);
    });

}

function simulateSelection()
{
    $('#carriercompare_errors').slideUp();
    $('#carriercompare_errors_list').children().remove();

    var query = $.ajax({
        type: 'POST',
        headers: {"cache-control": "no-cache"},
        url: baseDir + 'modules/carriercompare/ajax.php' + '?rand=' + new Date().getTime(),
        data: 'method=simulateSelection&' + $('#compare_shipping_form').serialize() + '&token=' + static_token,
        dataType: 'json',
        success: function (json) {
            if (json.price != 0)
            {
                var price = formatCurrency(json.price, currencyFormat, currencySign, currencyBlank);
                $('#total_shipping').html(price);
                var total = formatCurrency(json.order_total + json.price, currencyFormat, currencySign, currencyBlank);
                $('#total_price').html(total);
            } else
            {
                $('#total_shipping').html(txtFree);
                var total = formatCurrency(json.order_total, currencyFormat, currencySign, currencyBlank);
                $('#total_price').html(total);
            }

            $('#total_tax').html(formatCurrency(json.total_tax, currencyFormat, currencySign, currencyBlank));

            $('tr.cart_total_delivery').show();
        }
    });
    ajaxQueries.push(query);
}

function resetAjaxQueries()
{
    for (i = 0; i < ajaxQueries.length; ++i)
        ajaxQueries[i].abort();
    ajaxQueries = new Array();
}

// -------------------------------------------------------------------

function cc_updateDisplay()
{
    cc_removeMsg()

    cc_displayDimexact();

    cc_displayFreeShip();
}

function cc_removeMsg()
{
    $("#HOOK_SHOPPING_CART .msg").remove();
}

function cc_displayDimexact()
{
    //Délai allongé de 24h pour toute découpe sur mesure Dimexact
    if ($(".cart_item.dimexact").length > 0) {
        $("#order-opc_carrier .dimexact").remove();
        $("#order-opc_carrier .delivery_options_address").after('<div class="msg notice dimexact">' + txtTimeShip + '</div>');
    }
}

function cc_displayFreeShip()
{
    //Il manque x€ pour avoir une livraison gratuite
//	var free_ship = fpv_free_ship_relais;
    var total_cart_wt = 0;
    $("#cart_summary tr.cart_item .cart_total span.price").each(function () {
        total_cart_wt += parseFloat($(this).text().replace(/,/g, ".").replace(/ /g, ""));
    });

//	var diff_free_ship = (free_ship - total_cart_wt).toFixed(2);
//	$("#HOOK_SHOPPING_CART .freeship").remove();
//	if(diff_free_ship > 0) 
//		$("#HOOK_SHOPPING_CART").prepend('<div class="msg warning freeship" style="display:none;"><p>'+ txtNoFreeShip.replace(/%s/g, diff_free_ship.replace(/\./, ",")) +'</p></div>');
//	else
//		$("#HOOK_SHOPPING_CART").prepend('<div class="msg success freeship" style="display:none;"><p>'+ txtFreeShip.replace(/%s/g, free_ship.toFixed(2).replace(/\./, ",")) +'</p></div>');
//	
    $("#HOOK_SHOPPING_CART").find('.msg').slideDown('slow');
}

function cc_check()
{
    var $o = $("#carriers_list tr input[type='radio']:checked").parents(".item");

    cc_set($o);
}

function cc_set($o)
{
    $("#carriers_list .item").removeClass('checked');

    $o.addClass('checked');
    $o.find("input[type='radio']").prop('checked', true).trigger('change');

    id_carrier = $o.data('id');
}

function cc_setId(id)
{
    var $o = $("#carriers_list tr .item[data-id='" + id + "']")
    if ($o.length > 0)
        cc_set($o);
}

function cc_save()
{
    var sKey = "cc_id_carrier";
    var sValue = $("#carriers_list tr input[type='radio']:checked").parents(".item").data('id');
    var sExpires = "; max-age=300"; 	// Durée de vie de 5 minutes du cookie
    var sDomain = location.hostname;	// Restriction au domaine courant
    var sPath = location.pathname;		// Restriction au répertoire de commande
    var bSecure = location.protocol == "https:" ? true : false;

    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
}

function cc_load()
{
    var sKey = "cc_id_carrier";
    var sValue = decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

    cc_inLoad = false;

    if (sValue)
        cc_setId(sValue);
}



//end carrier file