{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$opc}
    {assign var='current_step' value='address'}
{capture name=path}{l s='Addresses'}{/capture}
{assign var="back_order_page" value="order.php"}
<h1 class="page-heading">{l s='Addresses'}</h1>
{include file="$tpl_dir./order-steps.tpl"}
{include file="$tpl_dir./errors.tpl"}
<form action="{$link->getPageLink($back_order_page, true)|escape:'html':'UTF-8'}" method="post">
{else}
    {assign var="back_order_page" value="order-opc.php"}
    {*    <div id="opc_account" class="opc-main-block">
    <div id="opc_account-overlay" class="opc-overlay" style="display: none;"></div>*}
    <div id="order-opc_address" class="order-opc_section"><div class="order-opc_section_content">
            <div id="opc_address_edit" class="opc-main-block fpv_content"></div>
        {/if}



        <div class="addresses clearfix">
            <h3 class="fpv_order_section_title fpv_icon fpv_icon_address">Adresse</h3>
            <div class="fpv_content">
                <p class="address_delivery select">
                    <label for="id_address_delivery">{if $cart->isVirtualCart()}{l s='Choose a billing address:'}{else}{l s='Choose a delivery address:'}{/if}</label>
                    <select name="id_address_delivery" id="id_address_delivery" class="address_select form-control">
                        {foreach from=$addresses key=k item=address}
                            <option value="{$address.id_address|intval}"{if $address.id_address == $cart->id_address_delivery} selected="selected"{/if}>
                                {$address.address1|escape:'html':'UTF-8'} {$address.postcode} {$address.city|escape:'html':'UTF-8'} {$address.country|escape:'html':'UTF-8'}
                            </option>
                        {/foreach}
                    </select><span class="waitimage"></span>
                </p>

                <p class="address_add submit">
                    <span class="fpv_button fpv_icon fpv_icon_plus" title="Ajouter">Ajouter une nouvelle adresse</span>
                </p>

                <div class="clearfix">
                    <ul class="address" id="address_delivery" style="height: 24px;">
                        {foreach from=$addresses key=k item=address}
                            {if $address.id_address == $cart->id_address_delivery}
                                <li class="address_title">Votre adresse de livraison</li>
                                <li class="address_firstname lastname">{$address.lastname|escape:'html':'UTF-8'} {$address.firstname|escape:'html':'UTF-8'}</li><li class="address_address1">Avenue Jean Jaurès</li>
                                <li class="address_address1">{$address.address1|escape:'html':'UTF-8'}</li>
                                <li class="address_postcode city">{$address.postcode|escape:'html':'UTF-8'} {$address.city|escape:'html':'UTF-8'}</li>     
                                <li class="address_Country:name">{$address.country|escape:'html':'UTF-8'} </li>
                                <li class="address_phone">{$address.phone} {$address.phone_mobile}</li>
                                <li class="address_update">
                                    <span data-id_address="{$address.id_address|intval}" title="Mettre à jour">» Mettre à jour</span></li>
                                {/if}
                            {/foreach}
                    </ul>
                    <div></div>
                </div>


            </div>


            <h3 class="fpv_order_section_title fpv_icon fpv_icon_invoice">Adresse de Facturation</h3>

            <div class="fpv_content">

                <p class="checkbox addressesAreEquals">
                    <input type="checkbox" name="same" id="addressesAreEquals" value="1"{if $cart->id_address_invoice == $cart->id_address_delivery || $addresses|@count == 1} checked="checked" onclick="updateAddressesDisplay();updateAddressSelection();" {/if} />
                    <label for="addressesAreEquals">Utiliser la même adresse pour la facturation</label>
                </p>

                <p id="address_invoice_form" style="display: none; padding-top:0" class="select">

                    <label for="id_address_invoice" class="strong">Choisissez une adresse de facturation :</label>
                    <select name="id_address_invoice" id="id_address_invoice" class="address_select" onchange="updateAddressesDisplay();
                        updateAddressSelection();" style="display: none;">
                        <option value="{$address.id_address|intval}">{$address.address1|escape:'html':'UTF-8'}</option>
                    </select>

                </p><p class="address_add address_add_invoice" style="display: none;">
                    <span class="fpv_button fpv_icon fpv_icon_plus" title="Ajouter">Ajouter une nouvelle adresse</span>
                </p>
                <p></p>

                <div class="clearfix">
                    <ul class="address " id="address_invoice" style="display: none; height: 24px;">
                        <li class="address_firstname lastname">{$cart->lastname|escape:'html':'UTF-8'} {$cart->firstname|escape:'html':'UTF-8'}</li>
                        <li class="address_address1">{$cart->address1|escape:'html':'UTF-8'}</li>
                        <li class="address_postcode city">{$cart->postcode|escape:'html':'UTF-8'} {$cart->city|escape:'html':'UTF-8'}</li>     
                        <li class="address_Country:name">{$cart->country|escape:'html':'UTF-8'} </li>
                        <li class="address_phone">{$cart->phone} {$cart->phone_mobile}</li>
                        <li class="address_update">
                            <span data-id_address="{$cart->id_address|intval}" title="Mettre à jour">» Mettre à jour</span></li>                
                    </ul>
                </div>

                <div id="invoice_same_delivery" class="msg msg-xs notice" style="display: none;">L'adresse de facturation sélectionnée est identique à l'adresse de livraison.</div>

            </div>

        </div>

    </div>






    <p class="cart_navigation">
        <a href="#livraison" class="multishipping-button multishipping-checkout exclusive" title="Continuer" style="display: none;">Continuer »</a>
    </p>
    {if !$opc}
        <div id="ordermsg" class="form-group">
            <label>{l s='If you would like to add a comment about your order, please write it in the field below.'}</label>
            <textarea class="form-control" cols="60" rows="6" name="message">{if isset($oldMessage)}{$oldMessage}{/if}</textarea>
        </div>
    {/if}<!-- end addresses -->
    {if !$opc}
        <p class="cart_navigation clearfix">
            <input type="hidden" class="hidden" name="step" value="2" />
            <input type="hidden" name="back" value="{$back}" />
            <a href="{$link->getPageLink($back_order_page, true, NULL, "{if $back}back={$back}{/if}")|escape:'html':'UTF-8'}" title="{l s='Previous'}" class="button-exclusive btn btn-default">
                <i class="icon-chevron-left"></i>
                {l s='Continue Shopping'}
            </a>
            <button type="submit" name="processAddress" class="button btn btn-default button-medium">
                <span>{l s='Proceed to checkout'}<i class="icon-chevron-right right"></i></span>
            </button>
        </p>
</form>
{else}
</div> <!--  end opc_account -->
{/if}
{strip}
    {if !$opc}
        {addJsDef orderProcess='order'}
        {addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
        {addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
        {addJsDefL name=CloseTxt}{l s='Submit' js=1}{/addJsDefL}
    {/if}
{capture}{if $back}&mod={$back|urlencode}{/if}{/capture}
{capture name=addressUrl}{$link->getPageLink('address', true, NULL, 'back='|cat:$back_order_page|cat:'?step=1'|cat:$smarty.capture.default)|escape:'quotes':'UTF-8'}{/capture}
{addJsDef addressUrl=$smarty.capture.addressUrl}
{capture}{'&multi-shipping=1'|urlencode}{/capture}
{addJsDef addressMultishippingUrl=$smarty.capture.addressUrl|cat:$smarty.capture.default}
{capture name=addressUrlAdd}{$smarty.capture.addressUrl|cat:'&id_address='}{/capture}
{addJsDef addressUrlAdd=$smarty.capture.addressUrlAdd}
{addJsDef formatedAddressFieldsValuesList=$formatedAddressFieldsValuesList}
{addJsDef opc=$opc|boolval}
{capture}<h3 class="page-subheading">{l s='Your billing address' js=1}</h3>{/capture}
{addJsDefL name=titleInvoice}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<h3 class="page-subheading">{l s='Your delivery address' js=1}</h3>{/capture}
{addJsDefL name=titleDelivery}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<a class="button button-small btn btn-default" href="{$smarty.capture.addressUrlAdd}" title="{l s='Update' js=1}"><span>{l s='Update' js=1}<i class="icon-chevron-right right"></i></span></a>{/capture}
            {addJsDefL name=liUpdate}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
        {/strip}

