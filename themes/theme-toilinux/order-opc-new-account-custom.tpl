<div id="opc_new_account" class="opc-main-block fpv_content">





    <div id="order-opc_customer-type" class="fpv_bouton_titre order-opc_section"><div class="order-opc_section_content">

            <div id="order-opc_btn_new_customer" class="fpv_bouton fpv_icon fpv_icon_new_user"> Vous êtes un nouveau client</div>

            <div id="order-opc_btn_old_customer" class="fpv_bouton fpv_icon fpv_icon_user">Vous êtes déja client</div>

        </div></div>





    <div id="opc_new_account-overlay" class="opc-overlay"></div>

    <div id="order-opc_login" class="order-opc_section"><div class="order-opc_section_content">

            <h2><span>1</span> Compte</h2>

            {*	<form action="{$link->getPageLink('authentication', true, NULL, "back=order-opc")|escape:'html':'UTF-8'}" method="post" id="login_form" class="std">*}
            <form action="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" method="post" id="login_form" class="std">
                <fieldset>

                    <h3>Déjà inscrit?</h3>

                    <!-- <p><a href="#" id="openLoginFormBlock">&raquo; Cliquez ici</a></p> -->

                    <div id="login_form_content">

                        <div id="order-opc_login_warning_email_exist" class="error" style="display:none;">L'adresse eMail est déjà utilisé par compte Client. Veuillez saisir le mot de passe.</div>

                        <div id="order-opc_login_password_send" class="msg success" style="display:none;">Le lien permettant de régénérer votre mot de passe vous a été envoyé à l'adresse email indiqué.</div>


                        <!-- Error return block -->
                        <div id="opc_login_errors" class="error" style="display:none;"></div>
                        <!-- END Error return block -->

                        <div class="fpv_50w">
                            <label for="login_email">Votre adresse eMail</label>
                            <span><input id="login_email" name="email" placeholder="Votre adresse eMail" class="opc-validate validate_email validate_required" type="email"></span>
                        </div>

                        <div class="fpv_50w">
                            <label for="login_passwd">Mot de passe</label>
                            <span><input id="login_passwd" name="login_passwd" placeholder="Mot de passe" class="opc-validate validate_required" type="password"></span>
<input id="SubmitLostPassword" class="lost_password fpv_button" value="Mot de passe oublié ?" type="button">
                        </div>

                        <p class="submit">
                            <input class="hidden" name="back" value="" type="hidden">					<input id="SubmitLogin" name="SubmitLogin" class="button" value="Connexion sécurisée" type="submit">
                        </p>

                    </div>
                </fieldset>
            </form>

        </div></div> <!-- End 'order-opc_login' -->



    <form action="javascript:;" method="post" id="new_account_form" class="std" autocomplete="on" autofill="on">
        <fieldset>

            <h3 id="new_account_title">Nouveau client</h3>

            <div id="opc_account_choice" style="display:none;">
                <div class="opc_float">
                    <h4>Commande instantanée</h4>
                    <p>
                        <input class="exclusive_large" id="opc_guestCheckout" value="Commander en tant qu'invité" type="button">
                    </p>
                </div>

                <div class="opc_float">
                    <h4>Créez votre compte dès aujourd'hui et profitez :</h4>
                    <ul class="bullet">
                        <li>Accès au site sécurisé et personnalisé</li>
                        <li>Commande rapide et facile</li>
                        <li>Adresse de livraison différente de celle de facturation</li>
                    </ul>
                    <p>
                        <input class="button_large" id="opc_createAccount" value="Créez votre compte" type="button">
                    </p>
                </div>
                <div class="clear"></div>
            </div>


            <div id="opc_account_form">





                <!-- Error return block -->
                <div id="opc_account_errors" class="error" style="display:none;"></div>
                <!-- END Error return block -->

                <!-- Account -->
                <input id="is_new_customer" name="is_new_customer" value="1" type="hidden">
                <input id="opc_id_customer" name="opc_id_customer" value="0" type="hidden">
                <input id="opc_id_address_delivery" name="opc_id_address_delivery" value="0" type="hidden">
                <input id="opc_id_address_invoice" name="opc_id_address_invoice" value="0" type="hidden">



                <div id="order-opc_signup_email" class="order-opc_section"><div class="order-opc_section_content">


                        <p class="required text fpv_100w">
                            <label for="email">Votre adresse eMail <sup>*</sup></label>
                            <input class="text opc-validate validate_email validate_required" id="email" name="email" value="" placeholder="Votre adresse eMail" type="email">
                        </p>

                        <p class="required password is_customer_param fpv_50w">
                            <label for="passwd">Mot de passe <sup>*</sup></label>
                            <input class="text" name="passwd" id="passwd" placeholder="Mot de passe" value="R0LLxcSF" type="password">
                            <sup class="sup_suffixe" style="display:none;">*</sup>

                            <span class="form_info">(5 characters min.)</span>
                        </p>


                        <p class="cart_navigation">
                            <input id="order-opc_signup_email_validation" class="multishipping-button multishipping-checkout exclusive large" title="Connexion sécurisée" value="Connexion sécurisée »" style="display: none;" type="button">
                        </p>


                    </div></div> <!-- End 'order-opc_signup_email' -->






                <div id="order-opc_signup_address" class="order-opc_section"><div class="order-opc_section_content">




                        <h3 class="fpv_order_section_title fpv_icon fpv_icon_address">Adresse</h3>



                        <p class="radio required fpv_100w">
                            <span>Civilité</span>
                            <input name="id_gender" id="id_gender1" value="1" type="radio">
                            <label for="id_gender1" class="top">M.</label>
                            <input name="id_gender" id="id_gender2" value="2" type="radio">
                            <label for="id_gender2" class="top">Mme</label>
                            <input name="id_gender" id="id_gender3" value="3" type="radio">
                            <label for="id_gender3" class="top">Melle</label>
                        </p>


                        <p class="required text fpv_50w" style="display:none;">
                            <label for="customer_lastname">Nom <sup>*</sup></label>
                            <input class="text" id="customer_lastname" name="customer_lastname" value="" placeholder="Nom" type="text">
                            <sup class="sup_suffixe" style="display:none;">*</sup>
                        </p>


                        <p class="required text fpv_50w" style="display:none;">
                            <label for="customer_firstname">Prénom <sup>*</sup></label>
                            <input class="text" id="customer_firstname" name="customer_firstname" value="" placeholder="Prénom" type="text">
                            <sup class="sup_suffixe" style="display:none;">*</sup>
                        </p>


                        <p class="select" style="display:none;">
                            <span>Date de naissance</span>
                            <select id="days" name="days">
                                <option value="">-</option>
                                <option value="1">1&nbsp;&nbsp;</option>
                                <option value="2">2&nbsp;&nbsp;</option>
                                <option value="3">3&nbsp;&nbsp;</option>
                                <option value="4">4&nbsp;&nbsp;</option>
                                <option value="5">5&nbsp;&nbsp;</option>
                                <option value="6">6&nbsp;&nbsp;</option>
                                <option value="7">7&nbsp;&nbsp;</option>
                                <option value="8">8&nbsp;&nbsp;</option>
                                <option value="9">9&nbsp;&nbsp;</option>
                                <option value="10">10&nbsp;&nbsp;</option>
                                <option value="11">11&nbsp;&nbsp;</option>
                                <option value="12">12&nbsp;&nbsp;</option>
                                <option value="13">13&nbsp;&nbsp;</option>
                                <option value="14">14&nbsp;&nbsp;</option>
                                <option value="15">15&nbsp;&nbsp;</option>
                                <option value="16">16&nbsp;&nbsp;</option>
                                <option value="17">17&nbsp;&nbsp;</option>
                                <option value="18">18&nbsp;&nbsp;</option>
                                <option value="19">19&nbsp;&nbsp;</option>
                                <option value="20">20&nbsp;&nbsp;</option>
                                <option value="21">21&nbsp;&nbsp;</option>
                                <option value="22">22&nbsp;&nbsp;</option>
                                <option value="23">23&nbsp;&nbsp;</option>
                                <option value="24">24&nbsp;&nbsp;</option>
                                <option value="25">25&nbsp;&nbsp;</option>
                                <option value="26">26&nbsp;&nbsp;</option>
                                <option value="27">27&nbsp;&nbsp;</option>
                                <option value="28">28&nbsp;&nbsp;</option>
                                <option value="29">29&nbsp;&nbsp;</option>
                                <option value="30">30&nbsp;&nbsp;</option>
                                <option value="31">31&nbsp;&nbsp;</option>
                            </select>

                            <select id="months" name="months">
                                <option value="">-</option>
                                <option value="1">Janvier&nbsp;</option>
                                <option value="2">Février&nbsp;</option>
                                <option value="3">Mars&nbsp;</option>
                                <option value="4">Avril&nbsp;</option>
                                <option value="5">Mai&nbsp;</option>
                                <option value="6">Juin&nbsp;</option>
                                <option value="7">Juillet&nbsp;</option>
                                <option value="8">Août&nbsp;</option>
                                <option value="9">Septembre&nbsp;</option>
                                <option value="10">Octobre&nbsp;</option>
                                <option value="11">Novembre&nbsp;</option>
                                <option value="12">Décembre&nbsp;</option>
                            </select>
                            <select id="years" name="years">
                                <option value="">-</option>
                                <option value="2017">2017&nbsp;&nbsp;</option>
                                <option value="2016">2016&nbsp;&nbsp;</option>
                                <option value="2015">2015&nbsp;&nbsp;</option>
                                <option value="2014">2014&nbsp;&nbsp;</option>
                                <option value="2013">2013&nbsp;&nbsp;</option>
                                <option value="2012">2012&nbsp;&nbsp;</option>
                                <option value="2011">2011&nbsp;&nbsp;</option>
                                <option value="2010">2010&nbsp;&nbsp;</option>
                                <option value="2009">2009&nbsp;&nbsp;</option>
                                <option value="2008">2008&nbsp;&nbsp;</option>
                                <option value="2007">2007&nbsp;&nbsp;</option>
                                <option value="2006">2006&nbsp;&nbsp;</option>
                                <option value="2005">2005&nbsp;&nbsp;</option>
                                <option value="2004">2004&nbsp;&nbsp;</option>
                                <option value="2003">2003&nbsp;&nbsp;</option>
                                <option value="2002">2002&nbsp;&nbsp;</option>
                                <option value="2001">2001&nbsp;&nbsp;</option>
                                <option value="2000">2000&nbsp;&nbsp;</option>
                                <option value="1999">1999&nbsp;&nbsp;</option>
                                <option value="1998">1998&nbsp;&nbsp;</option>
                                <option value="1997">1997&nbsp;&nbsp;</option>
                                <option value="1996">1996&nbsp;&nbsp;</option>
                                <option value="1995">1995&nbsp;&nbsp;</option>
                                <option value="1994">1994&nbsp;&nbsp;</option>
                                <option value="1993">1993&nbsp;&nbsp;</option>
                                <option value="1992">1992&nbsp;&nbsp;</option>
                                <option value="1991">1991&nbsp;&nbsp;</option>
                                <option value="1990">1990&nbsp;&nbsp;</option>
                                <option value="1989">1989&nbsp;&nbsp;</option>
                                <option value="1988">1988&nbsp;&nbsp;</option>
                                <option value="1987">1987&nbsp;&nbsp;</option>
                                <option value="1986">1986&nbsp;&nbsp;</option>
                                <option value="1985">1985&nbsp;&nbsp;</option>
                                <option value="1984">1984&nbsp;&nbsp;</option>
                                <option value="1983">1983&nbsp;&nbsp;</option>
                                <option value="1982">1982&nbsp;&nbsp;</option>
                                <option value="1981">1981&nbsp;&nbsp;</option>
                                <option value="1980">1980&nbsp;&nbsp;</option>
                                <option value="1979">1979&nbsp;&nbsp;</option>
                                <option value="1978">1978&nbsp;&nbsp;</option>
                                <option value="1977">1977&nbsp;&nbsp;</option>
                                <option value="1976">1976&nbsp;&nbsp;</option>
                                <option value="1975">1975&nbsp;&nbsp;</option>
                                <option value="1974">1974&nbsp;&nbsp;</option>
                                <option value="1973">1973&nbsp;&nbsp;</option>
                                <option value="1972">1972&nbsp;&nbsp;</option>
                                <option value="1971">1971&nbsp;&nbsp;</option>
                                <option value="1970">1970&nbsp;&nbsp;</option>
                                <option value="1969">1969&nbsp;&nbsp;</option>
                                <option value="1968">1968&nbsp;&nbsp;</option>
                                <option value="1967">1967&nbsp;&nbsp;</option>
                                <option value="1966">1966&nbsp;&nbsp;</option>
                                <option value="1965">1965&nbsp;&nbsp;</option>
                                <option value="1964">1964&nbsp;&nbsp;</option>
                                <option value="1963">1963&nbsp;&nbsp;</option>
                                <option value="1962">1962&nbsp;&nbsp;</option>
                                <option value="1961">1961&nbsp;&nbsp;</option>
                                <option value="1960">1960&nbsp;&nbsp;</option>
                                <option value="1959">1959&nbsp;&nbsp;</option>
                                <option value="1958">1958&nbsp;&nbsp;</option>
                                <option value="1957">1957&nbsp;&nbsp;</option>
                                <option value="1956">1956&nbsp;&nbsp;</option>
                                <option value="1955">1955&nbsp;&nbsp;</option>
                                <option value="1954">1954&nbsp;&nbsp;</option>
                                <option value="1953">1953&nbsp;&nbsp;</option>
                                <option value="1952">1952&nbsp;&nbsp;</option>
                                <option value="1951">1951&nbsp;&nbsp;</option>
                                <option value="1950">1950&nbsp;&nbsp;</option>
                                <option value="1949">1949&nbsp;&nbsp;</option>
                                <option value="1948">1948&nbsp;&nbsp;</option>
                                <option value="1947">1947&nbsp;&nbsp;</option>
                                <option value="1946">1946&nbsp;&nbsp;</option>
                                <option value="1945">1945&nbsp;&nbsp;</option>
                                <option value="1944">1944&nbsp;&nbsp;</option>
                                <option value="1943">1943&nbsp;&nbsp;</option>
                                <option value="1942">1942&nbsp;&nbsp;</option>
                                <option value="1941">1941&nbsp;&nbsp;</option>
                                <option value="1940">1940&nbsp;&nbsp;</option>
                                <option value="1939">1939&nbsp;&nbsp;</option>
                                <option value="1938">1938&nbsp;&nbsp;</option>
                                <option value="1937">1937&nbsp;&nbsp;</option>
                                <option value="1936">1936&nbsp;&nbsp;</option>
                                <option value="1935">1935&nbsp;&nbsp;</option>
                                <option value="1934">1934&nbsp;&nbsp;</option>
                                <option value="1933">1933&nbsp;&nbsp;</option>
                                <option value="1932">1932&nbsp;&nbsp;</option>
                                <option value="1931">1931&nbsp;&nbsp;</option>
                                <option value="1930">1930&nbsp;&nbsp;</option>
                                <option value="1929">1929&nbsp;&nbsp;</option>
                                <option value="1928">1928&nbsp;&nbsp;</option>
                                <option value="1927">1927&nbsp;&nbsp;</option>
                                <option value="1926">1926&nbsp;&nbsp;</option>
                                <option value="1925">1925&nbsp;&nbsp;</option>
                                <option value="1924">1924&nbsp;&nbsp;</option>
                                <option value="1923">1923&nbsp;&nbsp;</option>
                                <option value="1922">1922&nbsp;&nbsp;</option>
                                <option value="1921">1921&nbsp;&nbsp;</option>
                                <option value="1920">1920&nbsp;&nbsp;</option>
                                <option value="1919">1919&nbsp;&nbsp;</option>
                                <option value="1918">1918&nbsp;&nbsp;</option>
                                <option value="1917">1917&nbsp;&nbsp;</option>
                                <option value="1916">1916&nbsp;&nbsp;</option>
                                <option value="1915">1915&nbsp;&nbsp;</option>
                                <option value="1914">1914&nbsp;&nbsp;</option>
                                <option value="1913">1913&nbsp;&nbsp;</option>
                                <option value="1912">1912&nbsp;&nbsp;</option>
                                <option value="1911">1911&nbsp;&nbsp;</option>
                                <option value="1910">1910&nbsp;&nbsp;</option>
                                <option value="1909">1909&nbsp;&nbsp;</option>
                                <option value="1908">1908&nbsp;&nbsp;</option>
                                <option value="1907">1907&nbsp;&nbsp;</option>
                                <option value="1906">1906&nbsp;&nbsp;</option>
                                <option value="1905">1905&nbsp;&nbsp;</option>
                                <option value="1904">1904&nbsp;&nbsp;</option>
                                <option value="1903">1903&nbsp;&nbsp;</option>
                                <option value="1902">1902&nbsp;&nbsp;</option>
                                <option value="1901">1901&nbsp;&nbsp;</option>
                                <option value="1900">1900&nbsp;&nbsp;</option>
                            </select>
                        </p>


                        <h3 class="fpv_order_section_title fpv_icon fpv_icon_carrier" style="display:none;">Adresse de livraison</h3>



                        <p class="required text fpv_50w">
                            <label for="lastname">Nom <sup>*</sup></label>
                            <input class="text opc-validate validate_generic-name validate_required" id="lastname" name="lastname" value="" placeholder="Nom" onkeyup="$(this).val(this.value.toUpperCase());
                                                $('#customer_lastname').val(this.value);" onblur="$(this).val(this.value.toUpperCase());
                                                        $('#customer_lastname').val($(this).val());" type="text">
                            <sup class="sup_suffixe" style="display:none;">*</sup>
                        </p>



                        <p class="required text fpv_50w">
                            <label for="firstname">Prénom <sup>*</sup></label>
                            <input class="text opc-validate validate_generic-name validate_required" id="firstname" name="firstname" value="" placeholder="Prénom" onkeyup="$('#customer_firstname').val(this.value);" onblur="$('#customer_firstname').val($(this).val());" type="text">
                            <sup class="sup_suffixe" style="display:none;">*</sup>
                        </p>















                        <p class="text fpv_100w">
                            <label for="company">Société</label>
                            <input class="text" id="company" name="company" value="" placeholder="Société" type="text">
                        </p>








                        <div id="vat_number_block" class="fpv_50w" style="display:none;">
                            <p class="text">
                                <label for="vat_number">Numéro de TVA</label>
                                <input class="text" name="vat_number" id="vat_number" value="" placeholder="Numéro de TVA" style="display: none;" type="text">
                            </p>
                        </div>







                        <p class="required text fpv_100w" id="address_alias">
                            <span class="inline-infos msg notice" style="display: none;">Pendant votre saisie, une liste d'adresses vous est proposée afin de vous faciliter la saisie</span>
                            <input class="text opc-validate validate_generic-name" name="alias" id="alias" value="" placeholder="Votre adresse" autocomplete="off" type="text">
                            <!-- <sup class="sup_suffixe" style="display:none;">*</sup> -->
                        </p>
                        <input type="hidden" name="alias" id="alias" value="Mon adresse" />


                        <div class="order-opc_address-cell">

                            <p class="required text fpv_100w">
                                <label for="address1">Adresse <sup>*</sup></label>
                                <input class="text opc-validate validate_address validate_required" name="address1" id="address1" value="" placeholder="Adresse" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>







                            <p class="text is_customer_param fpv_100w">
                                <span class="inline-infos msg notice" style="display: none;">Compléments d'adresse (digicode, nom sur interphone, ...)</span>
                                <label for="address2">Complément d'adresse (Lieu dit, digicode, nom sur interphone, étage,..)</label>
                                <input class="text" name="address2" id="address2" value="" placeholder="Complément d'adresse (Lieu dit, digicode, nom sur interphone, étage,..)" type="text">
                            </p>







                            <p class="required postcode text fpv_25w" style="display: none;">
                                <label for="postcode">Code Postal <sup>*</sup></label>
                                <input class="text opc-validate validate_postcode validate_required" name="postcode" id="postcode" value="" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" placeholder="Code Postal" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>







                            <p class="required text fpv_75w">
                                <label for="city">Ville  <sup>*</sup></label>
                                <input class="text opc-validate validate_city validate_required" name="city" id="city" value="" placeholder="Ville " type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>







                            <p class="required select fpv_100w">
                                <label for="id_country">Pays <sup>*</sup></label>
                                <select name="id_country" id="id_country" class="form-control">
                                    {foreach from=$countries item=v}
                                        <option value="{$v.id_country}"{if (isset($guestInformations) && isset($guestInformations.id_country) && $guestInformations.id_country == $v.id_country) || (!isset($guestInformations) && $sl_country == $v.id_country)} selected="selected"{/if}>{$v.name|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>

                        </div><!-- End 'order-opc_address-cell' -->









                        <p class="required id_state select fpv_100w" style="display: none;">
                            <label for="id_state">État <sup>*</sup></label>
                            <select name="id_state" id="id_state">
                                <option value="">-</option>
                            </select>
                        </p>



                        <p class="required text dni fpv_50w" style="display: none;">
                            <label for="dni">Numéro d'identification fiscale</label>
                            <input class="text" name="dni" id="dni" value="" placeholder="Numéro d'identification fiscale" type="text">
                            <span class="form_info">DNI / NIF / NIE</span>
                        </p>



                        <p class="textarea is_customer_param fpv_100w">
                            <label for="other">Informations supplémentaires</label>
                            <textarea name="other" id="other" cols="26" rows="3" placeholder="Informations supplémentaires"></textarea>
                        </p>

                        <p class="infos-phone fpv_100w"><span class="inline-infos msg notice" style="display: none;">Merci de renseigner au moins un numéro de téléphone</span></p>



                        <p class="required text fpv_50w">
                            <label for="phone">Téléphone fixe</label>
                            <input class="text opc-validate validate_phone" name="phone" id="phone" value="" placeholder="Téléphone fixe" type="text">
                            <sup class="sup_suffixe" style="display:none;">*</sup>
                        </p>



                        <p class="text is_customer_param fpv_50w">
                            <label for="phone_mobile">Téléphone mobile</label>
                            <input class="text opc-validate validate_phone" name="phone_mobile" id="phone_mobile" value="" placeholder="Téléphone mobile" type="text">
                            <sup class="sup_suffixe" style="display:none;">*</sup>
                        </p>




                        <p class="checkbox fpv_100w">
                            <input name="newsletter" id="newsletter" value="1" type="checkbox">
                            <label for="newsletter">S'inscrire à notre newsletter</label>
                        </p>
                        <p class="checkbox fpv_100w">
                            <input name="optin" id="optin" value="1" type="checkbox">
                            <label for="optin">Recevoir des offres spéciales de nos partenaires</label>
                        </p>





                        <p class="checkbox is_customer_param">
                            <input name="invoice_address" id="invoice_address" type="checkbox">
                            <label for="invoice_address"><b>Je souhaite utiliser une autre adresse pour la facturation</b></label>
                        </p>


                        <div id="opc_invoice_address" class="is_customer_param" style="display: none;">

                            <h3 class="fpv_order_section_title fpv_icon fpv_icon_invoice">Adresse de facturation</h3>



                            <p class="required text fpv_50w">
                                <label for="lastname_invoice">Nom <sup>*</sup></label>
                                <input class="text opc-validate validate_generic-name validate_required" id="lastname_invoice" name="lastname_invoice" value="" placeholder="Nom" onkeyup="$(this).val(this.value.toUpperCase());" onblur="$(this).val(this.value.toUpperCase());" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>		


                            <p class="required text fpv_50w">
                                <label for="firstname_invoice">Prénom <sup>*</sup></label>
                                <input class="text opc-validate validate_generic-name validate_required" id="firstname_invoice" name="firstname_invoice" value="" placeholder="Prénom" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>		













                            <p class="text is_customer_param fpv_100w">
                                <label for="company_invoice">Société</label>
                                <input class="text" id="company_invoice" name="company_invoice" value="" placeholder="Société" type="text">
                            </p>



                            <p class="required text dni_invoice fpv_50w" style="display: none;">
                                <label for="dni">Numéro d'identification fiscale</label>
                                <input class="text" name="dni_invoice" id="dni_invoice" value="" placeholder="Numéro d'identification fiscale" type="text">
                                <span class="form_info">DNI / NIF / NIE</span>
                            </p>






                            <div id="vat_number_block_invoice" class="fpv_50w" style="display:none;">
                                <p class="text">
                                    <label for="vat_number_invoice">Numéro de TVA</label>
                                    <input class="text" id="vat_number_invoice" name="vat_number_invoice" value="" placeholder="Numéro de TVA" type="text">
                                </p>
                            </div>





                            <p class="required text fpv_100w" id="address_alias_invoice">
                                <span class="inline-infos msg notice" style="display: none;">Pendant votre saisie, une liste d'adresses vous est proposée afin de vous faciliter la saisie</span>
                                <input class="text opc-validate validate_required" name="alias_invoice" id="alias_invoice" value="" placeholder="Votre adresse de facturation" autocomplete="off" autofill="off" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>
                            <!-- <input type="hidden" name="alias_invoice" id="alias_invoice" value="Mon adresse de facturation" /> -->



                            <div class="order-opc_address-cell">

                                <p class="required text fpv_100w">
                                    <label for="address1_invoice">Adresse <sup>*</sup></label>
                                    <input class="text opc-validate validate_address validate_required" name="address1_invoice" id="address1_invoice" value="" placeholder="Adresse" type="text">
                                    <sup class="sup_suffixe" style="display:none;">*</sup>
                                </p>






                                <p class="text is_customer_param fpv_100w">
                                    <label for="address2_invoice">Complément d'adresse (Lieu dit, digicode, nom sur interphone, étage,..)</label>
                                    <input class="text" name="address2_invoice" id="address2_invoice" value="" placeholder="Complément d'adresse (Lieu dit, digicode, nom sur interphone, étage,..)" type="text">
                                </p>






                                <p class="required postcode text fpv_50w" style="display: none;">
                                    <label for="postcode_invoice">Code Postal <sup>*</sup></label>
                                    <input class="text opc-validate validate_postcode validate_required" name="postcode_invoice" id="postcode_invoice" value="" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" placeholder="Code Postal" type="text">
                                    <sup class="sup_suffixe" style="display:none;">*</sup>
                                </p>






                                <p class="required text fpv_50w">
                                    <label for="city_invoice">Ville  <sup>*</sup></label>
                                    <input class="text opc-validate validate_city validate_required" name="city_invoice" id="city_invoice" value="" placeholder="Ville " type="text">
                                    <sup class="sup_suffixe" style="display:none;">*</sup>
                                </p>






                                <p class="required select fpv_100w">
                                    <label for="id_country_invoice">Pays <sup>*</sup></label>
                                    <select name="id_country_invoice" id="id_country_invoice">
                                        <option value="">-</option>
                                        <option value="1">Allemagne</option>
                                        <option value="3">Belgique</option>
                                        <option value="69">Corse</option>
                                        <option value="6">Espagne</option>
                                        <option value="8" selected="selected">France (hors Corse et Dom)</option>
                                        <option value="98">Guadeloupe</option>
                                        <option value="241">Guyane Française</option>
                                        <option value="10">Italie</option>
                                        <option value="12">Luxembourg</option>
                                        <option value="141">Martinique</option>
                                        <option value="144">Mayotte</option>
                                        <option value="148">Monaco</option>
                                        <option value="242">Polynésie Française</option>
                                        <option value="15">Portugal</option>
                                        <option value="176">Réunion, Île de la</option>
                                        <option value="179">Saint-Barthélemy</option>
                                        <option value="182">Saint-Martin</option>
                                        <option value="19">Suisse</option>
                                    </select>
                                    <sup class="sup_suffixe" style="display:none;">*</sup>
                                </p>

                            </div><!-- End 'order-opc_address-cell' -->








                            <p class="required id_state_invoice select fpv_100w" style="display:none;">
                                <label for="id_state_invoice">État</label>
                                <select name="id_state_invoice" id="id_state_invoice">
                                    <option value="">-</option>
                                </select>
                                <sup>*</sup>
                            </p>



                            <p class="textarea is_customer_param fpv_100w">
                                <label for="other_invoice">Informations supplémentaires</label>
                                <textarea name="other_invoice" id="other_invoice" cols="26" rows="3" placeholder="Informations supplémentaires"></textarea>
                            </p>


                            <p class="infos-phone_invoice fpv_100w"><span class="inline-infos msg notice" style="display: none;">Merci de renseigner au moins un numéro de téléphone</span></p>


                            <p class="required text fpv_50w">
                                <label for="phone_invoice">Téléphone fixe</label>
                                <input class="text opc-validate validate_phone" name="phone_invoice" id="phone_invoice" value="" placeholder="Téléphone fixe" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>



                            <p class="text is_customer_param fpv_50w">
                                <label for="phone_mobile_invoice">Téléphone mobile</label>
                                <input class="text opc-validate validate_phone" name="phone_mobile_invoice" id="phone_mobile_invoice" value="" placeholder="Téléphone mobile" type="text">
                                <sup class="sup_suffixe" style="display:none;">*</sup>
                            </p>


                        </div> <!-- End 'opc_invoice_address' -->





                        <p id="new_account_form_no_valid" style="display:none;">
                            <span class="msg warning">Veuillez vérifier la validité des champs du formulaire !<br>Afin de continuer vers l'étape suivante : 'Livraison'.</span>
                        </p>


                        <p class="submit cart_navigation">
                            <input class="exclusive" name="submitAccount" id="submitAccount" value="Continuer" type="submit">

                        </p>


                        <p style="display: none;" id="opc_account_saved">
                            Les informations du compte ont été enregistrées avec succès
                        </p>


                        <p class="required opc-required" style="clear: both;">
                            <sup>*</sup>Champ obligatoire
                        </p>


                    </div></div> <!-- End 'order-opc_signup_address' -->

                <!-- END Account -->
            </div>
        </fieldset>
    </form>
    <div class="clear"></div>
</div>